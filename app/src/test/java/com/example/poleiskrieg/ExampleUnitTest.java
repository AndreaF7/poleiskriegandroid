package com.example.poleiskrieg;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void terrain_scan() {

        final String SCAN_ID = "TERRAIN";
        final String SCAN_ROOT = "com.example.poleiskrieg.logics.game.model.objects.terrains";
        final String TARGET_CLASS = "com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain";
        scan(SCAN_ID, SCAN_ROOT, TARGET_CLASS);

        assertEquals(1, 1);
    }


    @Test
    public void skill_tree_attributes_scan() {
        final String SCAN_ID = "SKILL_TREE_ATTRIBUTE";
        final String SCAN_ROOT = "com.example.poleiskrieg.logics.game.model.skilltree";
        final String TARGET_CLASS = "com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute";
        scan(SCAN_ID, SCAN_ROOT, TARGET_CLASS);

        assertEquals(1, 1);
    }

    @Test
    public void game_mode_scan() {
        final String SCAN_ID = "GAME_RULE";
        final String SCAN_ROOT = "com.example.poleiskrieg.logics.game.model.gamerules";
        final String TARGET_CLASS = "com.example.poleiskrieg.logics.game.model.gamerules.GameRules";
        scan(SCAN_ID, SCAN_ROOT, TARGET_CLASS);

        assertEquals(1, 1);
    }

    @Test
    public void game_object_scan() {
        final String SCAN_ID = "GAME_OBJECT";
        final String SCAN_ROOT = "com.example.poleiskrieg.logics.game.model.objects";
        final String TARGET_CLASS = "com.example.poleiskrieg.logics.game.model.objects.GameObject";
        scan(SCAN_ID, SCAN_ROOT, TARGET_CLASS);

        assertEquals(1, 1);
    }

    @Test
    public void race_scan() {
        final String SCAN_ID = "RACE";
        final String SCAN_ROOT = "com.example.poleiskrieg.logics.game.model.races";
        final String TARGET_CLASS = "com.example.poleiskrieg.logics.game.model.races.Race";
        scan(SCAN_ID, SCAN_ROOT, TARGET_CLASS);

        assertEquals(1, 1);
    }

    @Test
    public void structure_scan() {
        final String SCAN_ID = "STRUCTURE";
        final String SCAN_ROOT = "com.example.poleiskrieg.logics.game.model.objects.structures";
        final String TARGET_CLASS = "com.example.poleiskrieg.logics.game.model.objects.structures.Structure";
        scan(SCAN_ID, SCAN_ROOT, TARGET_CLASS);

        assertEquals(1, 1);
    }

    private void scan(String scanId, String scanRoot, String targetClass){
        final String FILE = "./src/main/res/raw/scan_classes.json";

        File tmpDir = new File(FILE);
        JSONObject classList = new JSONObject();
        if(tmpDir.exists()){
            //JSON parser object to parse read file
            JSONParser jsonParser = new JSONParser();

            try (FileReader reader = new FileReader(FILE))
            {
                //Read JSON file
                classList = (JSONObject) jsonParser.parse(reader);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //add the classes
        List<String> scanReslt = new ArrayList<>();
        try (ScanResult scanResult = new ClassGraph()
                .whitelistPackages(scanRoot)
                .enableAllInfo()
                .scan()) {
            for (String className :
                    scanResult.getClassesImplementing(targetClass)
                            .getNames()) {

                scanReslt.add(className);
            }
        }
        classList.put(scanId, scanReslt);


        //Write JSON file
        try (FileWriter file = new FileWriter(FILE)) {

            file.write(classList.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
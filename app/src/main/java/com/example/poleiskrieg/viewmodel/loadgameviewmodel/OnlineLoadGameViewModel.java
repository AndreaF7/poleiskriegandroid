package com.example.poleiskrieg.viewmodel.loadgameviewmodel;

import android.app.Application;
import android.text.SpannableStringBuilder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.repositories.loadgame.OnlineLoadGameRepository;
import com.example.poleiskrieg.util.C;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class OnlineLoadGameViewModel extends AndroidViewModel {
    private final OnlineLoadGameRepository repository;
    private int selectedIndex;
    private MutableLiveData<Boolean> isInfoVisible;
    private MutableLiveData<Boolean> isConfirmDeleteVisible;
    private MutableLiveData<Boolean> isJoinVisible;
    private LiveData<List<GameWithPlayers>> gameList;
    private final Application application;

    public OnlineLoadGameViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        this.repository = new OnlineLoadGameRepository(application);
        this.isInfoVisible = new MutableLiveData<>();
        this.isConfirmDeleteVisible = new MutableLiveData<>();
        this.isJoinVisible = new MutableLiveData<>();
        this.isJoinVisible.setValue(false);
        this.isInfoVisible.setValue(false);
        this.isConfirmDeleteVisible.setValue(false);
        this.selectedIndex = 0;
        this.gameList = this.repository.getGames();
    }

    public MutableLiveData<Boolean> getIsInfoVisible() {
        return this.isInfoVisible;
    }

    public void setIsInfoVisible(final boolean isCreditsVisible) {
        this.isInfoVisible.setValue(isCreditsVisible);
    }

    public MutableLiveData<Boolean> getIsJoinVisible() { return this.isJoinVisible; }

    public void setIsJoinVisible(final boolean isJoinVisible) {
        this.isJoinVisible.setValue(isJoinVisible);
    }

    public int getPlayerIdInGame() {
            return Objects.requireNonNull(this.gameList.getValue()).get(selectedIndex).playerEntityList.stream().filter(x -> x.playerAccountId.equals(C.getLoggedPlayerUsername().get())).findFirst().get().id;
    }

    public LiveData<List<GameWithPlayers>> getOnlineGames(){
        return this.gameList;
    }

    public LiveData<Long> searchOnlineGame(long onlineId) { return this.repository.searchOnlineGame(onlineId);}

    public void setIsConfirmDeleteVisible(final boolean isConfirmDeleteVisible) {
        this.isConfirmDeleteVisible.setValue(isConfirmDeleteVisible);
    }

    public MutableLiveData<Boolean> getIsConfirmDeleteVisible(){
        return this.isConfirmDeleteVisible;
    }

    public void setPosition(int position){
        this.selectedIndex = position;
    }

    public long getGameId(){
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).gameEntity.id;
    }

    public long getOnlineId() {
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).gameEntity.onlineId;
    }

    public void deleteGame(){
        repository.deleteGame(getGameId());
        //this.gameList.setValue(repository.getOnlineGames()); TODO uncomment
    }

    public boolean isPlayerListFull() {
        return this.gameList.getValue().get(this.selectedIndex).gameEntity.numberOfPlayers == this.gameList.getValue().get(selectedIndex)
                .playerEntityList.size();
    }

    public String getGameName(){
        return getSelectedGameEntity().gameName;
    }

    public boolean isCreator() {
        return this.gameList.getValue().get(this.selectedIndex).gameEntity.creatorId.equals(C.getLoggedPlayerUsername().get());
    }

    public int getCurrentPlayerId() {
        return this.gameList.getValue().get(this.selectedIndex).gameEntity.turnPlayerId;
    }

    public SpannableStringBuilder getDescriptionMsg(){
        Map<Object, Optional<Object>> map = new LinkedHashMap<>();
        map.put(R.string.game_info_name, Optional.of(getGameName()));
        map.put(R.string.game_info_online_id, Optional.of(Long.toString(getOnlineId())));
        map.put(R.string.game_info_game_rule, Optional.of(getSelectedGameEntity().gameRules.getName()));
        map.put(R.string.game_info_game_date, Optional.of(getSelectedGameEntity().dateSaved.toString()));
        map.put(R.string.game_info_number_of_player, Optional.of(Integer.toString(getSelectedGameEntity().numberOfPlayers)));
        map.put(R.string.game_info_players, Optional.empty());

        getPlayerEntityList().forEach(playerEntity -> {
            map.put(playerEntity.name, Optional.of(playerEntity.race.getRaceNameId()));
        });
        return C.savedataToString(map, getApplication().getApplicationContext());
    }

    private GameEntity getSelectedGameEntity(){
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).gameEntity;
    }

    private List<PlayerEntity> getPlayerEntityList(){
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).playerEntityList;
    }
}

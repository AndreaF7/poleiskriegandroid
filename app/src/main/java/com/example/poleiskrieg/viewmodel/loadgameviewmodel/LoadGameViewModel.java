package com.example.poleiskrieg.viewmodel.loadgameviewmodel;

import android.app.Application;
import android.text.SpannableStringBuilder;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.repositories.loadgame.OfflineLoadGameRepository;
import com.example.poleiskrieg.util.C;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class LoadGameViewModel  extends AndroidViewModel {

    private final OfflineLoadGameRepository repository;
    private int selectedIndex;
    private MutableLiveData<Boolean> isInfoVisible;
    private MutableLiveData<Boolean> isConfirmDeleteVisible;
    private LiveData<List<GameWithPlayers>> gameList;

    public LoadGameViewModel(@NonNull Application application) {
        super(application);
        this.repository = new OfflineLoadGameRepository(application);
        this.isInfoVisible = new MutableLiveData<>();
        this.isConfirmDeleteVisible = new MutableLiveData<>();
        this.isInfoVisible.setValue(false);
        this.isConfirmDeleteVisible.setValue(false);
        this.selectedIndex = 0;
        this.gameList = repository.getGames();
    }

    public MutableLiveData<Boolean> getIsInfoVisible() {
        return this.isInfoVisible;
    }

    public void setIsInfoVisible(final boolean isCreditsVisible) {
        this.isInfoVisible.setValue(isCreditsVisible);
    }

    public LiveData<List<GameWithPlayers>> getLocalGames(){
        return this.gameList;
    }

    public void setIsConfirmDeleteVisible(final boolean isConfirmDeleteVisible) {
        this.isConfirmDeleteVisible.setValue(isConfirmDeleteVisible);
    }

    public MutableLiveData<Boolean> getIsConfirmDeleteVisible(){
        return this.isConfirmDeleteVisible;
    }

    public void setPosition(int position){
        this.selectedIndex = position;
    }

    public long getGameId(){
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).gameEntity.id;
    }

    public void deleteGame(){
        repository.deleteGame(getGameId());
    }

    public String getGameName(){
        return getSelectedGameEntity().gameName;
    }

    public SpannableStringBuilder getDescriptionMsg(){
        Map<Object, Optional<Object>> map = new LinkedHashMap<>();
        map.put(R.string.game_info_name, Optional.of(getGameName()));
        map.put(R.string.game_info_game_rule, Optional.of(getSelectedGameEntity().gameRules.getName()));
        map.put(R.string.game_info_game_date, Optional.of(getSelectedGameEntity().dateSaved.toString()));
        map.put(R.string.game_info_number_of_player, Optional.of(Integer.toString(getSelectedGameEntity().numberOfPlayers)));
        map.put(R.string.game_info_players, Optional.empty());

        getPlayerEntityList().forEach(playerEntity -> {
            map.put(playerEntity.name, Optional.of(playerEntity.race.getRaceNameId()));
        });
        return C.savedataToString(map, getApplication().getApplicationContext());
    }

    private GameEntity getSelectedGameEntity(){
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).gameEntity;
    }

    private List<PlayerEntity> getPlayerEntityList(){
        return Objects.requireNonNull(this.gameList.getValue()).get(this.selectedIndex).playerEntityList;
    }
}
package com.example.poleiskrieg.viewmodel.Factories;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class GameViewModelFactory implements ViewModelProvider.Factory{
    private Application mApplication;
    private long gameId;
    private int playerId;


    public GameViewModelFactory(Application application, long gameId, int playerId) {
        this.mApplication = application;
        this.gameId = gameId;
        this.playerId = playerId;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new GameViewModel(mApplication, gameId, playerId);
    }
}

package com.example.poleiskrieg.viewmodel.playerlist;

import android.app.Application;

import androidx.annotation.NonNull;

import com.example.poleiskrieg.repositories.playerlist.PlayerListRepository;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.player.PlayerImpl;


public class PlayersListViewModel extends AbstractPlayersListViewModel {

    private PlayerListRepository repository;

    public PlayersListViewModel(@NonNull Application application, long gameId) {
        super(application, gameId);
        this.repository = new PlayerListRepository(application);
        super.setGameWithPlayers(this.repository.getGameWithPlayers(gameId));
    }

    @Override
    public long getOnlineId() {
        return -1;
    }

    @Override
    public long getPlayerIdInGame() {
        return -1;
    }

    public long createGame(){
        return this.repository.createGame(this.gameId);
    }

    /**
     * Method that checks if the player can be added, and if it can then adds it.
     * The player will not be added if he has a race and / or a name already
     * selected by another player.
     *
     * @param id            is the player id.
     * @param name          is the name of the player to add.
     * @param className     is the race of the player to add.
     * @return true if the player has been added.
     */
    public boolean canAddThePlayer(final int id, final String name, final String className) {
        if (players.stream().map(p -> p.getName()).anyMatch(n -> n.equals(name))
                || players.stream().map(p -> p.getRace().getClass().getSimpleName()).anyMatch(n -> n.equals(className))) {
            return false;
        }
        Player player = new PlayerImpl(name, id + 1,
                getRaceList().stream().filter(r -> r.getClass().getSimpleName().equals(className)).findFirst().get(),
                selectedGameMode.generateObjective());
        this.players.add(player);
        this.repository.addPlayer(player, gameId);
        return true;
    }

}

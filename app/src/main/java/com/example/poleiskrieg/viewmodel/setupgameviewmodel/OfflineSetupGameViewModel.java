package com.example.poleiskrieg.viewmodel.setupgameviewmodel;


import android.app.Application;
import android.content.Intent;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.PlayersListActivity;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.repositories.setupgame.SetupGameRepository;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


public class OfflineSetupGameViewModel extends AndroidViewModel implements SetupGameViewModel{

    private static final String GAME_NAME_INCIPIT = "Game%s";
    private static final int MINIMUM_PLAYERS = 2;
    private static final int MAXIMUM_PLAYERS = 4;
    private static final int INITIAL_GAME_RULE_INDEX = 0;

    private final SetupGameRepository repository;
    private int count;
    private MutableLiveData<Integer> playersNum;
    private MutableLiveData<GameRules> actualGameMode;
    private EditText gameNameEditText;

    public OfflineSetupGameViewModel(@NonNull Application application, EditText gameNameEditText) {
        super(application);
        this.gameNameEditText = gameNameEditText;
        this.count = INITIAL_GAME_RULE_INDEX;
        this.playersNum = new MutableLiveData<>();
        this.actualGameMode = new MutableLiveData<>();

        this.playersNum.setValue(MINIMUM_PLAYERS);
        this.actualGameMode.setValue(getGameRules().get(count));
        this.repository = new SetupGameRepository(application);
        gameNameEditText.setText(String.format(GAME_NAME_INCIPIT, new SimpleDateFormat("ddMMyy").format(new Date())));

    }

    public LiveData<Long> createGame(){
        return new MutableLiveData<Long>(this.repository.createGame(gameNameEditText.getText().toString(), this.getPlayersNum().getValue(), this.getGameMode().getValue()));
    }

    /**
     * @return the possible GameRules to be chosen
     */
    public MutableLiveData<GameRules> getGameMode() {
        return this.actualGameMode;
    }

    /**
     * @param circular if true get game rule in a circle.
     */
    public void setNewGameMode(final boolean circular) {
        this.actualGameMode.setValue(this.getGameRules().get(nextRuleIndex(circular)));
    }

    public int getActualGameModeNameId(){
        return Objects.requireNonNull(this.actualGameMode.getValue()).getName();
    }

    public int getActualGameModeDescriptionId(){
        return Objects.requireNonNull(this.actualGameMode.getValue()).getDescription();
    }

    public String getActualGameModeClassName(){
        return Objects.requireNonNull(this.actualGameMode.getValue().getClass().getCanonicalName());
    }

    /**
     * @return the number of players.
     */
    public MutableLiveData<Integer> getPlayersNum() {
        return this.playersNum;
    }

    /**
     * @param increment true if increase by one the players num. false if decrase.
     */
    public void changePlayersNum(boolean increment) {
        if(increment && this.playersNum.getValue() < MAXIMUM_PLAYERS){
            this.playersNum.setValue(this.playersNum.getValue()+1);
        } else if (!increment && this.playersNum.getValue() > MINIMUM_PLAYERS){
            this.playersNum.setValue(this.playersNum.getValue()-1);
        }
    }

    /**
     * @return the minimum number of players that could be set.
     */
    public int getMinimumPlayers() {
        return MINIMUM_PLAYERS;
    }

    /**
     * @return the maximum number of players that could be set.
     */
    public int getMaximumPlayers() {
        return MAXIMUM_PLAYERS;
    }

    public EditText getGameNameEditText(){return this.gameNameEditText;}

    /**
     * @return the list of GameRules implemented
     */
    private List<GameRules> getGameRules() {
        final List<GameRules> gameRules = new LinkedList<>();
        ClassScan.get().getGameRuleClasses().forEach(s -> {
            try {
                gameRules.add((GameRules) Class.forName(s).newInstance());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return Collections.unmodifiableList(gameRules);
    }

    private int nextRuleIndex(boolean increment){
        int size = getGameRules().size();
        return count = (increment) ? (count+1)%size : (count+(size-1))%size;
    }

}

package com.example.poleiskrieg.viewmodel;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;

import java.util.ArrayList;
import java.util.List;

public class TutorialViewModel extends AndroidViewModel {

    private MutableLiveData<Boolean> isTutorialVisible;
    private MutableLiveData<Boolean> isTutorialMenuVisible;
    private MutableLiveData<String> tutorialTitleText;
    private MutableLiveData<String> selectedInfo;
    private MutableLiveData<String> selectedInfoText;
    private MutableLiveData<Integer> selectedImage;
    private List<String> info;
    private List<String> texts;
    private List<Integer> images;

    public TutorialViewModel(@NonNull Application application) {
        super(application);
        this.isTutorialVisible = new MutableLiveData<>();
        this.isTutorialMenuVisible = new MutableLiveData<>();
        this.tutorialTitleText = new MutableLiveData<>();
        this.selectedInfo = new MutableLiveData<>();
        this.selectedInfoText = new MutableLiveData<>();
        this.selectedImage = new MutableLiveData<>();
        this.info = new ArrayList<>();
        this.texts = new ArrayList<>();
        this.images = new ArrayList<>();
        this.isTutorialVisible.setValue(false);
        this.isTutorialMenuVisible.setValue(false);
        this.selectedInfoText.setValue("");
        this.tutorialTitleText.setValue("");
        this.selectedInfo.setValue("");
        this.info.add("");
        this.texts.add("");
        this.images.add(-1);
    }

    public MutableLiveData<Boolean> getIsTutorialVisible() {
        return isTutorialVisible;
    }

    public void setIsTutorialVisible(boolean isTutorialVisible) {
        this.isTutorialVisible.setValue(isTutorialVisible);
    }

    public MutableLiveData<Boolean> getIsTutorialMenuVisible() {
        return isTutorialMenuVisible;
    }

    public void setIsTutorialMenuVisible(boolean isTutorialMenuVisible) {
        this.isTutorialMenuVisible.setValue(isTutorialMenuVisible);
    }

    public MutableLiveData<String> getTutorialTitleText() {
        return tutorialTitleText;
    }

    public void setTutorialTitleText(String tutorialTitleText) {
        this.tutorialTitleText.setValue(tutorialTitleText);
    }

    public MutableLiveData<String> getSelectedInfo() {
        return selectedInfo;
    }

    public MutableLiveData<String> getSelectedInfoText() {
        return selectedInfoText;
    }

    public void setInfo(List<String> info) {
        this.info = info;
        this.selectedInfo.setValue(this.info.get(0));
    }

    public List<String> getInfo() {
        return this.info;
    }

    public void setTexts(List<String> texts) {
        this.texts = texts;
        this.selectedInfoText.setValue(this.texts.get(0));
    }

    public void setImages(List<Integer> images) {
        this.images = images;
        this.selectedImage.setValue(this.images.get(0));
    }

    public LiveData<Integer> getSelectedImage() {
        return this.selectedImage;
    }

    public void changeInfo(boolean increment) {
        if(increment && this.info.indexOf(this.selectedInfo.getValue()) + 1 < this.info.size()) {
            this.selectedInfo.setValue(this.info.get(this.info.indexOf(this.selectedInfo.getValue()) + 1));
            this.selectedInfoText.setValue(this.texts.get(this.texts.indexOf(this.selectedInfoText.getValue()) + 1));
            this.selectedImage.setValue(this.images.get(this.images.indexOf(this.selectedImage.getValue()) + 1));
        } else if(!increment && this.info.indexOf(this.selectedInfo.getValue()) > 0) {
            this.selectedInfo.setValue(this.info.get(this.info.indexOf(this.selectedInfo.getValue()) - 1));
            this.selectedInfoText.setValue(this.texts.get(this.texts.indexOf(this.selectedInfoText.getValue()) - 1));
            this.selectedImage.setValue(this.images.get(this.images.indexOf(this.selectedImage.getValue()) - 1));
        }
    }
}

package com.example.poleiskrieg.viewmodel.Factories;

import android.app.Application;
import android.widget.EditText;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.setupgameviewmodel.OfflineSetupGameViewModel;

public class SetupGameViewModelFactory implements ViewModelProvider.Factory{
    private Application mApplication;
    private EditText playerNameEditText;


    public SetupGameViewModelFactory(Application application, EditText playerNameEditText) {
        this.mApplication = application;
        this.playerNameEditText = playerNameEditText;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new OfflineSetupGameViewModel(mApplication, playerNameEditText);
    }
}
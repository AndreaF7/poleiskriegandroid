package com.example.poleiskrieg.viewmodel.gameviewmodel;

import android.app.Application;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.MainActivity;
import com.example.poleiskrieg.activities.OnlineLoadGameActivity;
import com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils;
import com.example.poleiskrieg.logics.game.controller.map.VariableCasePart;
import com.example.poleiskrieg.logics.game.controller.map.VariablePartMapBuilder;
import com.example.poleiskrieg.logics.game.controller.selection.GameCommandsUsingSelection;
import com.example.poleiskrieg.logics.game.controller.selection.GameCommandsUsingSelectionImpl;
import com.example.poleiskrieg.logics.game.controller.selection.Selection;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.structures.Carpentry;
import com.example.poleiskrieg.logics.game.model.objects.structures.City;
import com.example.poleiskrieg.logics.game.model.objects.structures.CityImpl;
import com.example.poleiskrieg.logics.game.model.objects.structures.Mine;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.resources.Resource;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;
import com.example.poleiskrieg.logics.game.util.Coordinates;
import com.example.poleiskrieg.logics.game.util.Pair;
import com.example.poleiskrieg.logics.game.util.mapbuilder.GameMapBuilder;
import com.example.poleiskrieg.logics.game.util.mapbuilder.GameMapBuilderFactory;
import com.example.poleiskrieg.repositories.game.GameRepository;
import com.example.poleiskrieg.util.C;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils.modelObjectToViewId;
import static com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils.modelSelectionToViewId;

public class GameViewModel extends AndroidViewModel {

    private final GameCommandsUsingSelection model;
    private final GameMapBuilderFactory<Map<Coordinates, VariableCasePart>> gameMapBuilderFactory;
    private final Map<Coordinates, String> mapBase;
    private List<Integer> orientationScreenModes;
    private final MutableLiveData<Map<Coordinates, VariableCasePart>> mapState;
    private final MutableLiveData<List<VariableCasePart>> mapStateAsList;
    private final MutableLiveData<Player> actualPlayer;
    private final MutableLiveData<Optional<GameObject>> selectedGameObject;
    private final MutableLiveData<List<Unit>> possibleUnits;
    private final MutableLiveData<List<SkillTreeAttribute>> skillTreeUpgradableAttribute;
    private final MutableLiveData<Map<Resource, String>> resourceDescriptionMap;
    private MutableLiveData<Integer> orientationScreenIndex;
    private final MutableLiveData<Boolean> isStartTurnInfoVisible;
    private final MutableLiveData<Boolean> isOnPlayerInfoVisible;
    private final MutableLiveData<Boolean> isOnSelectionVisible;
    private final MutableLiveData<Boolean> isOnResourceVisible;
    private final MutableLiveData<Boolean> isOnCreateVisible;
    private final MutableLiveData<Boolean> isOnSkilltreeVisible;
    private final MutableLiveData<Boolean> isOnInfoVisible;
    private final MutableLiveData<Boolean> isNextTurnWin;
    private final MutableLiveData<Boolean> isGameMenuVisible;
    private final MutableLiveData<Boolean> winnerInfoVisible;
    private final MutableLiveData<String> infoTitle;
    private final MutableLiveData<SpannableStringBuilder> infoDescription;
    private int playerId;

    public GameViewModel(@NonNull Application application, GameCommandsUsingSelection model, int playerId) {
        super(application);
        this.model = model;
        this.gameMapBuilderFactory = () -> new VariablePartMapBuilder(this.model.getMapSize());
        this.mapBase = new HashMap<>();
        this.orientationScreenModes = C.getOrientationScreenModes();
        this.mapState = new MutableLiveData<>();
        this.mapStateAsList = new MutableLiveData<>();
        this.actualPlayer = new MutableLiveData<>();
        this.selectedGameObject = new MutableLiveData<>();
        this.possibleUnits = new MutableLiveData<>();
        this.skillTreeUpgradableAttribute = new MutableLiveData<>();
        this.resourceDescriptionMap = new MutableLiveData<>();
        this.isStartTurnInfoVisible = new MutableLiveData<>();
        this.isOnSelectionVisible = new MutableLiveData<>();
        this.isOnPlayerInfoVisible = new MutableLiveData<>();
        this.isOnCreateVisible = new MutableLiveData<>();
        this.isOnResourceVisible = new MutableLiveData<>();
        this.isOnSkilltreeVisible = new MutableLiveData<>();
        this.isOnInfoVisible = new MutableLiveData<>();
        this.infoTitle = new MutableLiveData<>();
        this.infoDescription = new MutableLiveData<>();
        this.winnerInfoVisible = new MutableLiveData<>();
        this.isNextTurnWin = new MutableLiveData<>();
        this.isGameMenuVisible = new MutableLiveData<>();
        this.orientationScreenIndex = new MutableLiveData<>(0); //TODO: prendere dal DB

        this.isOnCreateVisible.setValue(false);
        this.isOnSkilltreeVisible.setValue(false);
        this.isOnInfoVisible.setValue(false);
        this.winnerInfoVisible.setValue(false);
        this.isStartTurnInfoVisible.setValue(true);
        this.isGameMenuVisible.setValue(true);

        this.initMapBase();
        this.playerId = playerId;
        this.updateLiveData();
    }

    public GameViewModel(@NonNull Application application, long gameId, int playerId){
        this(application, new GameCommandsUsingSelectionImpl(application, gameId), playerId);
    }


    public Map<Coordinates, String> getTerrains() {
        return this.mapBase;
    }

    public List<String> getTerrainsAsList() {
        return getTerrains().entrySet().stream().sorted((o1, o2) -> {
            if (o1.getKey().getY() - o2.getKey().getY() != 0)
                return o1.getKey().getY() - o2.getKey().getY();
            else return o1.getKey().getX() - o2.getKey().getX();
        }).map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public LiveData<Map<Coordinates, VariableCasePart>> getActualMapState() {
        return this.mapState;
    }

    public LiveData<List<VariableCasePart>> getActualMapStateAsList() {
        return this.mapStateAsList;
    }

    public Pair<Integer, Integer> getMapSize() {
        return this.model.getMapSize();
    }

    public void cellHit(Coordinates cords) {
        if(playerId == -1 || getCurrentPlayer().getValue().getId() == playerId){
            this.model.selectPosition(cords);
            updateLiveData();
        }
    }

    private void initMapBase() {
        for (int i = 0; i < this.model.getMapSize().getKey(); i++) {
            for (int j = 0; j < this.model.getMapSize().getValue(); j++) {
                final Coordinates cords = new Coordinates(i, j);
                mapBase.put(cords, modelObjectToViewId(this.model.getTerrain(cords)));
            }
        }
    }

    public void nextTurn() {
        this.model.nextTurn();
        this.updateLiveData();

    }

    public LiveData<Boolean> getIsNextTurnWin() {
        return this.isNextTurnWin;
    }

    public LiveData<Player> getCurrentPlayer() {
        return this.actualPlayer;
    }

    public LiveData<Optional<GameObject>> getSelectedGameObject() {
        return this.selectedGameObject;
    }

    public void emptySelectedGameObject() {
        this.model.resetSelection();
        updateLiveData();
    }

    public LiveData<Boolean> getIsOnPlayerInfoVisible() {
        return this.isOnPlayerInfoVisible;
    }

    public LiveData<Boolean> getIsOnSelectionVisible() {
        return this.isOnSelectionVisible;
    }

    public LiveData<Boolean> getIsOnResourceVisible() {
        return this.isOnResourceVisible;
    }

    public LiveData<Boolean> getIsOnCreateVisible() {
        return this.isOnCreateVisible;
    }

    public void setIsOnCreateVisible(Boolean isOnCreateVisible) {
        this.isOnCreateVisible.setValue(isOnCreateVisible);
        updateLiveData();
    }

    public LiveData<Boolean> getIsStartTurnInfoVisible() {
        return this.isStartTurnInfoVisible;
    }

    public void setIsStartTurnInfoVisible(Boolean isStartTurnInfoVisible) {
        this.isStartTurnInfoVisible.setValue(isStartTurnInfoVisible);
        updateLiveData();
    }

    public MutableLiveData<Boolean> getWinnerInfoVisible() {
        return winnerInfoVisible;
    }

    public void setWinnerInfoVisible(Boolean winnerInfoVisible) {
        this.winnerInfoVisible.setValue(winnerInfoVisible);
        if(isNextTurnWin.getValue() && !winnerInfoVisible){
            deleteGame();
        }
        updateLiveData();
    }

    public LiveData<Boolean> getIsOnInfoVisible() {
        return this.isOnInfoVisible;
    }

    public void setIsOnInfoVisible(Boolean isOnInfoVisible) {
        this.isOnInfoVisible.setValue(isOnInfoVisible);
    }

    public LiveData<Boolean> getIsOnSkilltreeVisible() {
        return this.isOnSkilltreeVisible;
    }

    public void setIsOnSkilltreeVisible(Boolean isOnSkilltreeVisible) {
        this.isOnSkilltreeVisible.setValue(isOnSkilltreeVisible);
        updateLiveData();
    }

    public LiveData<Boolean> getIsGameMenuVisible() {
        return this.isGameMenuVisible;
    }

    public void setIsGameMenuVisible(Boolean isGameMenuVisible) {
        this.isGameMenuVisible.setValue(isGameMenuVisible);
        updateLiveData();
    }


    /*  INFO DIALOG */

    public MutableLiveData<String> getInfoTitle() {
        return infoTitle;
    }

    public void setInfoTitle(int title){
        this.infoTitle.setValue(getApplication().getResources().getString(title));
    }

    public void setInfoTitle(String title){
        this.infoTitle.setValue(title);
    }

    public MutableLiveData<SpannableStringBuilder> getInfoDescription() {
        return infoDescription;
    }

    public void setInfoDescription(Map<Integer, Optional<String>> description, boolean isInstanced){
        this.infoDescription.setValue(C.descriptionToString(description, getApplication().getApplicationContext(), isInstanced));
    }

    public void setInfoDescription(SpannableStringBuilder info){
        this.infoDescription.setValue(info);
    }

    public String getActualPlayerName(){
        return Objects.requireNonNull(this.actualPlayer.getValue()).getName();
    }

    public SpannableStringBuilder getActualPlayerInfo(){
        Map<Object, Optional<Object>> map = new LinkedHashMap<>();
        map.put(R.string.player_info_race, Optional.of(Objects.requireNonNull(actualPlayer.getValue()).getRace().getRaceNameId()));
        map.put(R.string.player_info_cities, Optional.of(Integer.toString(getActualPlayerSpecificStructureCount(CityImpl.class))));
        map.put(R.string.player_info_mines, Optional.of(Integer.toString(getActualPlayerSpecificStructureCount(Mine.class))));
        map.put(R.string.player_info_carpentry, Optional.of(Integer.toString(getActualPlayerSpecificStructureCount(Carpentry.class))));
        return C.playerInfoToString(map, getApplication().getApplicationContext());
    }

    private int getActualPlayerSpecificStructureCount(Class<? extends Structure> structureClass){
        return this.model.getActualPlayerSpecificStructureCount(structureClass);
    }


    /* CREATE */

    public LiveData<List<Unit>> getPossibleUnit() {
        return this.possibleUnits;
    }

    public boolean canCreate() {
        return this.model.canSelectedCityCreate();
    }

    public boolean canCreateUnit(Unit unit) {
        return this.model.canCreateUnit(unit);
    }

    public boolean isHeroNotInGame(Unit unit){
        return this.model.isHeroNotInGame(unit);
    }

    public void createUnit(int index) {
        this.model.createUnitFromSelectedCity(Objects.requireNonNull(this.possibleUnits.getValue()).get(index));
    }


    /* SKILLTREE */

    public LiveData<List<SkillTreeAttribute>> getSkilltreeUpgradableAttributes() {
        return this.skillTreeUpgradableAttribute;
    }

    public void setSkilltreeAttributeObjDescription(int position) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        this.setInfoDescription(this.model.getAttributeNewObjectDescription(position), false);
    }

    public int getSkilltreeAttributeImageId(SkillTreeAttribute skillTreeAttribute) {
        return C.getResId(ModelToViewConverterUtils.modelAttributeToViewId(skillTreeAttribute, getCurrentPlayer().getValue()), R.drawable.class);
    }

    public boolean canUpgradeAttribute(SkillTreeAttribute skillTreeAttribute) {
        return this.model.canUpgradeAttribute(skillTreeAttribute);
    }

    public void upgradeAttribute(int index) {
        this.model.upgradeAttribute(Objects.requireNonNull(this.skillTreeUpgradableAttribute.getValue()).get(index));
    }


    /* BEGIN TURN DIALOG */

    public SpannableStringBuilder getActualPlayerNameTitle(){
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append(getApplication().getResources().getString(R.string.start_turn_title_begin)).append(" ").append((Objects.requireNonNull(this.actualPlayer.getValue())).getName()).append(getApplication().getResources().getString(R.string.start_turn_title_end));
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD),
                getApplication().getResources().getString(R.string.start_turn_title_begin).length() + 1,
                stringBuilder.length()-getApplication().getResources().getString(R.string.start_turn_title_end).length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return stringBuilder;
    }

    public Integer getActualPlayerRaceImageId(){
        return C.getResId(ModelToViewConverterUtils.modelRaceToViewId(Objects.requireNonNull(this.actualPlayer.getValue()).getRace(),false), R.drawable.class);
    }


    /* WINNER DIALOG */

    public SpannableStringBuilder getWinnerText(){
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append((Objects.requireNonNull(this.actualPlayer.getValue())).getName()).append(" ").append(getApplication().getResources().getString(R.string.win_info_title));
        stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0,
                (Objects.requireNonNull(this.actualPlayer.getValue())).getName().length()-1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return stringBuilder;
    }


    /* ORIENTATION */

    public MutableLiveData<Integer> getOrientationScreenIndex() {
        return this.orientationScreenIndex;
    }

    public void setOrientationScreenIndex(final boolean increment) {
        this.orientationScreenIndex.setValue(nextIndex(increment, orientationScreenIndex.getValue(), orientationScreenModes.size()));
    }

    public Integer getOrientationScreenMode(){
        return this.orientationScreenModes.get(this.orientationScreenIndex.getValue());
    }

    /* PRIVATE */

    private int nextIndex(boolean increment, int actualPosition, int size){
        return (increment) ? (actualPosition+1)%size : (actualPosition+(size-1))%size;
    }

    public LiveData<Map<Resource, String>> getResourcesDescriptionList() {
        return this.resourceDescriptionMap;
    }


    private void deleteGame(){
        this.model.deleteGame();
    }

    protected void updateLiveData() {

        /**************************** CURRENT PLAYER ***************************/
        this.actualPlayer.setValue(this.model.getCurrentPlayer());

        /********************* MAP **********************/

        final GameMapBuilder<Map<Coordinates, VariableCasePart>> mapBuilder = this.gameMapBuilderFactory.get();
        for (int i = 0; i < this.model.getMapSize().getKey(); i++) {
            for (int j = 0; j < this.model.getMapSize().getValue(); j++) {
                final Coordinates cords = new Coordinates(i, j);
                final Optional<Unit> unit = this.model.getUnit(cords);
                final Optional<Structure> structure = this.model.getStructure(cords);
                final Optional<Selection> selection = this.model.getCaseSelection(cords);//TODO si puo' ottimizzare, il live data sono solo i singoli pezzi e vengono settati sol ose sono diversi da quelli prima
                structure.ifPresent(value -> mapBuilder.setBottom(cords, modelObjectToViewId(value)));
                unit.ifPresent(value -> mapBuilder.setTop(cords, modelObjectToViewId(value)));
                selection.ifPresent(value -> mapBuilder.setBorder(cords, modelSelectionToViewId(value)));
            }
        }
        this.mapState.setValue(mapBuilder.build());
        this.mapStateAsList.setValue(Objects.requireNonNull(this.mapState.getValue()).entrySet().stream()
                .sorted((o1, o2) -> {
                    if (o1.getKey().getY() - o2.getKey().getY() != 0)
                        return o1.getKey().getY() - o2.getKey().getY();
                    else return o1.getKey().getX() - o2.getKey().getX();
                })
                .map(Map.Entry::getValue)
                .collect(Collectors.toList())
        );

        /**************************** NEXT TURN WIN ***************************/
        this.isNextTurnWin.setValue(this.model.isNextTurnWin());

        /**************************** SELECTED OBJ *****************************/
        this.selectedGameObject.setValue(this.model.getActualSelection().isPresent() ? Optional.of(this.model.getActualSelection().get().getValue()) : Optional.empty());

        /**************************** POSSIBLE UNITS ***************************/
        this.possibleUnits.setValue(this.model.getPossibleUnit());

        /**************************** SKILLTREE ATTRIBUTES *********************/
        this.skillTreeUpgradableAttribute.setValue(this.model.getSkillTreeUpgradableAttribute());

        /**************************** RESOURCES ********************************/
        this.resourceDescriptionMap.setValue(this.model.getCurrentPlayerResourcesDescriptionMap());

        /**************************** VISIBILITY *******************************/
        this.isOnResourceVisible.setValue(true);
        if(playerId != -1 && getCurrentPlayer().getValue().getId() != playerId) {
            this.isOnSelectionVisible.setValue(false);
            this.isOnPlayerInfoVisible.setValue(false);
            this.isOnSkilltreeVisible.setValue(false);
            this.isGameMenuVisible.setValue(false);
            this.isOnInfoVisible.setValue(false);
            this.isOnResourceVisible.setValue(false);
            this.isNextTurnWin.setValue(false);
            this.isOnCreateVisible.setValue(false);
//            getApplication().startActivity(new Intent(getApplication(), OnlineLoadGameActivity.class));
//            return;
        } else if (this.isOnCreateVisible.getValue()) {
            this.isOnSelectionVisible.setValue(false);
            this.isOnPlayerInfoVisible.setValue(false);
            this.isOnSkilltreeVisible.setValue(false);
            this.isStartTurnInfoVisible.setValue(false);
            this.isGameMenuVisible.setValue(false);
            if (this.isOnInfoVisible.getValue()) {
                this.isOnInfoVisible.setValue(false);
            }
        } else if (this.isOnSkilltreeVisible.getValue()) {
            this.isOnSelectionVisible.setValue(false);
            this.isOnPlayerInfoVisible.setValue(false);
            this.isOnCreateVisible.setValue(false);
            this.isStartTurnInfoVisible.setValue(false);
            this.isGameMenuVisible.setValue(false);
            if (this.isOnInfoVisible.getValue()) {
                this.isOnInfoVisible.setValue(false);
            }
        } else if (this.isOnInfoVisible.getValue()) {
            this.isOnSelectionVisible.setValue(false);
            this.isOnPlayerInfoVisible.setValue(false);
            this.isOnCreateVisible.setValue(false);
            this.isOnSkilltreeVisible.setValue(false);
            this.isStartTurnInfoVisible.setValue(false);
            this.isGameMenuVisible.setValue(false);
        } else if (this.isStartTurnInfoVisible.getValue()) {
            this.isOnSelectionVisible.setValue(false);
            this.isOnPlayerInfoVisible.setValue(false);
            this.isOnCreateVisible.setValue(false);
            this.isOnSkilltreeVisible.setValue(false);
            this.isOnInfoVisible.setValue(false);
            this.isOnResourceVisible.setValue(false);
            this.isGameMenuVisible.setValue(false);
        } else if (this.isGameMenuVisible.getValue()) {
            this.isOnSelectionVisible.setValue(false);
            this.isOnPlayerInfoVisible.setValue(false);
            this.isOnCreateVisible.setValue(false);
            this.isOnSkilltreeVisible.setValue(false);
            this.isOnInfoVisible.setValue(false);
            this.isOnResourceVisible.setValue(false);
            this.isStartTurnInfoVisible.setValue(false);
        } else {
            if (this.model.getActualSelection().isPresent()) {
                this.isOnSelectionVisible.setValue(true);
                this.isOnPlayerInfoVisible.setValue(false);
            } else {
                this.isOnSelectionVisible.setValue(false);
                this.isOnPlayerInfoVisible.setValue(true);
            }
        }
    }

}

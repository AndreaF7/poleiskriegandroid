package com.example.poleiskrieg.viewmodel.Factories;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.playerlist.OnlinePlayerListViewModel;

public class OnlinePlayerListViewModelFactory implements ViewModelProvider.Factory {
    private Application application;
    private long gameId;

    public OnlinePlayerListViewModelFactory(Application application, long gameId){
        this.application = application;
        this.gameId = gameId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OnlinePlayerListViewModel(application, gameId);
    }
}

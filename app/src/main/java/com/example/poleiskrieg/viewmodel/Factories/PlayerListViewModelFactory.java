package com.example.poleiskrieg.viewmodel.Factories;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.playerlist.PlayersListViewModel;

public class PlayerListViewModelFactory implements ViewModelProvider.Factory{
        private Application mApplication;
        //private EditText playerNameEditText;
        private long gameId;


        public PlayerListViewModelFactory(Application application, long gameId) {
            this.mApplication = application;
            //this.playerNameEditText = playerNameEditText;
            this.gameId = gameId;
        }


        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new PlayersListViewModel(mApplication, gameId);
        }
}

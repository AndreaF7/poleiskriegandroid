package com.example.poleiskrieg.viewmodel.setupgameviewmodel;

import android.widget.EditText;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;

import java.util.Objects;

public interface SetupGameViewModel {

    LiveData<Long> createGame();

    /**
     * @return the possible GameRules to be chosen
     */
    MutableLiveData<GameRules> getGameMode();

    /**
     * @param circular if true get game rule in a circle.
     */
    void setNewGameMode(final boolean circular);

    int getActualGameModeNameId();

    int getActualGameModeDescriptionId();

    String getActualGameModeClassName();

    /**
     * @return the number of players.
     */
    MutableLiveData<Integer> getPlayersNum();

    /**
     * @param increment true if increase by one the players num. false if decrase.
     */
    void changePlayersNum(boolean increment);

    /**
     * @return the minimum number of players that could be set.
     */
    int getMinimumPlayers();

    /**
     * @return the maximum number of players that could be set.
     */
    int getMaximumPlayers();

    EditText getGameNameEditText();

}

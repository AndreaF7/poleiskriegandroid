package com.example.poleiskrieg.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.repositories.OnlineAccountRepository;
import com.example.poleiskrieg.util.C;

import java.util.List;
import java.util.Optional;

public class MainViewModel extends AndroidViewModel {

    private OnlineAccountRepository repository;
    //private Map<String, Integer> languages;
    //private int languageIndex;
    //private MutableLiveData<String> languageCode;
    private List<Integer> orientationScreenModes;
    private MutableLiveData<Integer> orientationScreenIndex;
    private MutableLiveData<Boolean> isCreditsVisible;
    private MutableLiveData<Boolean> isSettingsVisible;
    private MutableLiveData<Boolean> isSignInVisible;
    private MutableLiveData<Boolean> isRegisterVisible;
    private MutableLiveData<Boolean> isLoggedIn;
    private Integer ArrayList;

    public MainViewModel(@NonNull Application application) {
        super(application);
        this.repository = new OnlineAccountRepository(application);
        //this.languages = C.getSupportedLanguages();
        //this.languageCode= new MutableLiveData<>();
        //this.languageCode.setValue(Locale.getDefault().getLanguage());
        //this.languageIndex = getIndex();
        this.orientationScreenModes = C.getOrientationScreenModes();
        this.orientationScreenIndex= new MutableLiveData<>();
        this.isCreditsVisible = new MutableLiveData<>();
        this.isSettingsVisible = new MutableLiveData<>();
        this.isRegisterVisible = new MutableLiveData<>();
        this.isSignInVisible = new MutableLiveData<>();
        this.isLoggedIn = new MutableLiveData<>();
        this.isCreditsVisible.setValue(false);
        this.isSettingsVisible.setValue(false);
        this.isSignInVisible.setValue(false);
        this.isRegisterVisible.setValue(false);
        this.isLoggedIn.setValue(false);
        this.orientationScreenIndex.setValue(0);
    }

    public LiveData<Boolean> registerAccount(String account, String password) {
        return this.repository.registerAccount(account, password);
    }

    public LiveData<Boolean> logAccount(String account, String password){
        return this.repository.logAccount(account, password);
    }

    public void logOut() {
        this.repository.logOut();
    }

    /* SETTINGS */

    public MutableLiveData<Boolean> getIsSettingsVisible() {
        return this.isSettingsVisible;
    }

    public void setIsSettingsVisible(final boolean isSettingsVisible) {
        this.isSettingsVisible.setValue(isSettingsVisible);
    }

    /* ORIENTATION */

    public MutableLiveData<Integer> getOrientationScreenIndex() {
        return this.orientationScreenIndex;
    }

    public void setOrientationScreenIndex(final boolean increment) {
        this.orientationScreenIndex.setValue(nextIndex(increment, orientationScreenIndex.getValue(), orientationScreenModes.size()));
    }

    public Integer getOrientationScreenMode(){
        return this.orientationScreenModes.get(this.orientationScreenIndex.getValue());
    }

    /* LANGUAGE */

    /*public MutableLiveData<String> getLanguageCode() {
        return this.languageCode;
    }

    public Integer getLanguageName(){
        for(Map.Entry<String, Integer> e : languages.entrySet()) {
            if(e.getKey().equals(languageCode.getValue())){
                return e.getValue();
            }
        }
        return R.string.system_auto;
    }

    public void changeLanguage(boolean increment){
        this.languageIndex = nextIndex(increment, languageIndex, languages.size());
        this.languageCode.setValue((String) this.languages.keySet().toArray()[languageIndex]);
    }

    private int getIndex(){
        return new LinkedList<>(this.languages.keySet()).indexOf(this.languageCode.getValue());
    }

    private void setAppLanguage() {
        Locale locale = new Locale(Objects.requireNonNull(this.languageCode.getValue()));
        Locale.setDefault(locale);
        Configuration config = getApplication().getBaseContext().getResources().getConfiguration();
        DisplayMetrics metrics = getApplication().getBaseContext().getResources().getDisplayMetrics();
        config.setLocale(locale);
    }*/


    /* CREDITS */

    public MutableLiveData<Boolean> getIsCreditsVisible() {
        return this.isCreditsVisible;
    }

    public void setIsCreditsVisible(final boolean isCreditsVisible) {
        this.isCreditsVisible.setValue(isCreditsVisible);
    }

    public int getCreditsTitle(){
        return R.string.credits_title;
    }

    public int getCreditsMsg(){return R.string.credits_description;}

    /* SIGN */

    public MutableLiveData<Boolean> getIsSignInVisible() { return this.isSignInVisible; }

    public void setIsSignInVisible(final boolean isSignInVisible) {
        this.isSignInVisible.setValue(isSignInVisible);
    }

    public MutableLiveData<Boolean> getIsRegisterVisible() {
        return isRegisterVisible;
    }

    public void setIsRegisterVisible(final boolean isRegisterVisible) {
        this.isRegisterVisible.setValue(isRegisterVisible);
    }

    public void setIsLoggedIn(final boolean isLoggedIn) {
        this.isLoggedIn.setValue(isLoggedIn);
    }

    public MutableLiveData<Boolean> getIsLoggedIn() { return this.isLoggedIn;}

    /* PRIVATE */

    private int nextIndex(boolean increment, int actualPosition, int size){
        return (increment) ? (actualPosition+1)%size : (actualPosition+(size-1))%size;
    }

}

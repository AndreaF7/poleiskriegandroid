package com.example.poleiskrieg.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.MainViewModel;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;

import java.security.cert.PKIXRevocationChecker;
import java.util.Objects;
import java.util.Optional;

public class SettingsMenuFragment extends Fragment {

    private MainViewModel model;
    private TutorialViewModel tutorialViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_settings_menu, container, false);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        this.tutorialViewModel = new ViewModelProvider(requireActivity()).get(TutorialViewModel.class);
        TextView orientationText = view.findViewById(R.id.orientationScreenBtnText);
        /*TextView languageText = view.findViewById(R.id.languageBtnText);*/
        LiveData<Boolean> isSettingsVisible = model.getIsSettingsVisible();
        isSettingsVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        LiveData<Integer> orientation = model.getOrientationScreenIndex();
        orientation.observe(this.getViewLifecycleOwner(), bool -> {
            orientationText.setText(this.model.getOrientationScreenMode());
            switch (this.model.getOrientationScreenMode()){
                case R.string.screen_portrait:
                    Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case R.string.screen_landscape:
                    Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                default:
                    Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

            }
        });

        /*LiveData<String> languageCode = model.getLanguageCode();
        languageCode.observe(this.getViewLifecycleOwner(), bool -> {
            languageText.setText(this.model.getLanguageName());
        });*/
        setUI(view);
    }

    private void setUI(View view){
        /*view.findViewById(R.id.leftLanguageBtn).setOnClickListener(v->{
            C.doClickFeeling(view);
            this.model.changeLanguage(false);
            ((MainActivity)getActivity()).setAppLanguage(model.getLanguageCode().getValue());
        });
        view.findViewById(R.id.rightLanguageBtn).setOnClickListener(v->{
            C.doClickFeeling(view);
            this.model.changeLanguage(true);
        });*/
        view.findViewById(R.id.leftOrientationBtn).setOnClickListener(v->{
            C.doClickFeeling(view);
            this.model.setOrientationScreenIndex(false);
        });
        view.findViewById(R.id.rightOrintationBtn).setOnClickListener(v->{
            C.doClickFeeling(view);
            this.model.setOrientationScreenIndex(true);
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            this.model.setIsSettingsVisible(false);
        });
        view.findViewById(R.id.tutorialButton).setOnClickListener(v -> {
            this.tutorialViewModel.setIsTutorialVisible(true);
        });
    }
}

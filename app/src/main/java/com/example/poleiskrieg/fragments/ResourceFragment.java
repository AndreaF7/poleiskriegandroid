package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.adapter.ButtonTextImageAdapterLinear;
import com.example.poleiskrieg.element.ButtonTextImage;
import com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils;
import com.example.poleiskrieg.logics.game.model.resources.Resource;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

import java.util.ArrayList;
import java.util.Map;

public class ResourceFragment extends Fragment implements ButtonTextImageAdapterLinear.OnButtonClickListener {

    private GameViewModel model;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gamemap_resource, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewResources);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager;
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        ButtonTextImageAdapterLinear<String> adapter = new ButtonTextImageAdapterLinear<>(new ArrayList<>(), this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LiveData<Map<Resource, String>> resourcesDescription = model.getResourcesDescriptionList();
        resourcesDescription.observe(this.getViewLifecycleOwner(), d -> {
            ArrayList<ButtonTextImage<String>> list = new ArrayList<>();
            d.entrySet().forEach(e -> {
                list.add(new ButtonTextImage<String>(C.getResId(ModelToViewConverterUtils.modelResourceToViewId(e.getKey()), R.drawable.class), e.getValue(), C.TOP));
            });
            adapter.setList(list);
        });
    }

    @Override
    public void onButtonClick(int position) {}
}
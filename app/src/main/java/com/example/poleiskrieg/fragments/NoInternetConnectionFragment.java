package com.example.poleiskrieg.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.MainActivity;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class NoInternetConnectionFragment extends Fragment {
    private GameViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_dialog_title_image, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        TextView title = view.findViewById(R.id.dialogTitleText);
        ImageView image = view.findViewById(R.id.dialogImage);

        LiveData<Boolean> isStartTurnInfoVisible = model.getIsStartTurnInfoVisible();
        isStartTurnInfoVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
                title.setText(this.model.getActualPlayerNameTitle());
                image.setImageResource(this.model.getActualPlayerRaceImageId());
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        LiveData<Boolean> winnerInfoVisible = model.getWinnerInfoVisible();
        winnerInfoVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
                title.setText(this.model.getWinnerText());
                image.setImageResource(this.model.getActualPlayerRaceImageId());
            }
        });

        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            if(isStartTurnInfoVisible.getValue()){
                this.model.setIsStartTurnInfoVisible(false);
            } else {
                //this.model.deleteGame();
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                view.getContext().startActivity(intent);
            }
        });
    }

}

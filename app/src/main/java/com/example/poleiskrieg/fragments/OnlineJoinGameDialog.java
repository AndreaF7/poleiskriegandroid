package com.example.poleiskrieg.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.OnlinePlayerListActivity;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.OnlineLoadGameViewModel;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class OnlineJoinGameDialog extends Fragment {

    private OnlineLoadGameViewModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_online_join_game_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(Objects.requireNonNull(getActivity())).get(OnlineLoadGameViewModel.class);
        AtomicLong onlineId = new AtomicLong();
        model.getIsJoinVisible().observe(this.getViewLifecycleOwner(), bool -> {
            view.setVisibility(bool ? View.VISIBLE : View.INVISIBLE);
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            model.setIsJoinVisible(false);
        });
        view.findViewById(R.id.joinButton).setOnClickListener(v -> {
            if (!((EditText) view.findViewById(R.id.messageText)).getText().toString().equals("")) {
                onlineId.set(Long.parseLong(((EditText) view.findViewById(R.id.messageText)).getText().toString()));
            } else {
                onlineId.set(-1);
            }
            model.searchOnlineGame(onlineId.get()).observe(getViewLifecycleOwner(), localId -> {
                if (localId >= 0) {
                    Intent intent = new Intent(view.getContext(), OnlinePlayerListActivity.class);
                    intent.putExtra(C.GAME_ID, localId);
                    view.getContext().startActivity(intent);
                } else if(localId == -2){
                    Toast.makeText(getContext(), R.string.invalid_online_id, Toast.LENGTH_SHORT).show();
                } else if(localId == -3) {
                    Toast.makeText(getContext(), R.string.error_join_game_full, Toast.LENGTH_LONG).show();
                } else if(localId == -4) {
                    Toast.makeText(getContext(), R.string.error_join_already_in, Toast.LENGTH_LONG).show();
                }
            });
        });
    }
}

package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.adapter.CreateMenuElementAdapter;
import com.example.poleiskrieg.element.CreateMenuElement;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class SkilltreeMenuFragment extends Fragment implements CreateMenuElementAdapter.OnButtonClickListener {

    private GameViewModel model;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gamemap_shop_menu, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewResources);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager;
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        CreateMenuElementAdapter adapter = new CreateMenuElementAdapter(new ArrayList<>(), this, getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LiveData<Boolean> isOnSkilltree = model.getIsOnSkilltreeVisible();
        isOnSkilltree.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });

        LiveData<List<SkillTreeAttribute>> skilltreeAttributes = model.getSkilltreeUpgradableAttributes();
        skilltreeAttributes.observe(this.getViewLifecycleOwner(), attributes -> {
            ArrayList<CreateMenuElement> list = new ArrayList<>();
            attributes.stream().forEach(attribute -> {
                list.add(new CreateMenuElement(this.model.getSkilltreeAttributeImageId(attribute), attribute.getAttributeNameId(), attribute.getCost(), R.string.unlock_text, this.model.canUpgradeAttribute(attribute)));
            });
            adapter.setList(list);
        });

        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            dismissMenu();
        });
    }

    @Override
    public void onButtonClick(int position, int buttonId) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, java.lang.InstantiationException, IllegalAccessException {
        if(buttonId == R.id.createButton){
            this.model.upgradeAttribute(position);
            dismissMenu();
        } else if (buttonId == R.id.infoButton){
            this.model.setInfoTitle(this.model.getSkilltreeUpgradableAttributes().getValue().get(position).getAttributeLevelNameId());
            this.model.setSkilltreeAttributeObjDescription(position);
            this.model.setIsOnInfoVisible(true);
        }
    }

    private void dismissMenu(){
        this.model.emptySelectedGameObject();
        this.model.setIsOnSkilltreeVisible(false);
    }
}
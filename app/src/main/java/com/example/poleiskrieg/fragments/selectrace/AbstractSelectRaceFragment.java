package com.example.poleiskrieg.fragments.selectrace;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.OnlineLoadGameActivity;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.viewmodel.playerlist.PlayerListViewModelInterface;

public abstract class AbstractSelectRaceFragment extends Fragment {

    protected PlayerListViewModelInterface model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_dialog_title_text_button, container, false);
        return view;
    }

    public abstract PlayerListViewModelInterface getModel();

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = getModel();
        TextView title = view.findViewById(R.id.dialogTitleText);
        TextView message = view.findViewById(R.id.messageText);
        ButtonWithFeeling selectedButton = view.findViewById(R.id.joinButton);
        LiveData<Boolean> isSelectionMenuVisible = model.getIsSelectionMenuVisible();
        isSelectionMenuVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
                title.setText(this.model.getRaceList().get(this.model.getSelectedPosition()).getRaceNameId());
                message.setText(this.model.getRaceList().get(this.model.getSelectedPosition()).getRaceDescriptionId());
                if(this.model.isRaceAlreadySelected()){
                    selectedButton.setVisibility(View.INVISIBLE);
                } else {
                    selectedButton.setVisibility(View.VISIBLE);
                }
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            this.model.setIsSelectionMenuVisible(false);
        });
        this.setSelectButtonListener(view);
    }

    abstract void setSelectButtonListener(View view);
}

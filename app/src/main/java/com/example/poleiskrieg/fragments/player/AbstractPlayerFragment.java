package com.example.poleiskrieg.fragments.player;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.adapter.ButtonTextImageAdapterGrid;
import com.example.poleiskrieg.element.ButtonTextImage;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.playerlist.PlayerListViewModelInterface;
import com.example.poleiskrieg.viewmodel.playerlist.PlayersListViewModel;

import java.util.ArrayList;

public abstract class AbstractPlayerFragment extends Fragment implements ButtonTextImageAdapterGrid.OnButtonClickListener{

    private static final String TITLE_INCIPIT = "PLAYER %s";

    private PlayerListViewModelInterface model;
    private ArrayList<ButtonTextImage<Integer>> list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_player_setup, container, false);
        return view;
    }

    public abstract PlayerListViewModelInterface setViewModel();

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = setViewModel();
        this.model.setPlayerNameEditText(view.findViewById(R.id.playerNameText)); //TODO dalla factory l'ho dovuto spostare qui
        TextView titleText = view.findViewById(R.id.titleText);
        titleText.setText(String.format(TITLE_INCIPIT, this.model.getActualPlayerId()));

        /* ArrayList */
        this.list = new ArrayList<>();
        this.model.getRaceList().forEach(race -> {
            list.add(new ButtonTextImage<>(this.model.getRaceImageId(race), race.getRaceNameId(),C.TOP));
        });

        /* Recycler View setup */
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewResources);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            layoutManager= new GridLayoutManager(getActivity(), 2);
        } else {
            layoutManager = new GridLayoutManager(getActivity(), 4);
        }
        ButtonTextImageAdapterGrid<Integer> adapter = new ButtonTextImageAdapterGrid<>(list, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onButtonClick(int position) {
        this.model.setSelectedPosition(position);
        this.model.setIsSelectionMenuVisible(true);
    }
}

package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.adapter.CreateMenuElementAdapter;
import com.example.poleiskrieg.element.CreateMenuElement;
import com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

import java.util.ArrayList;
import java.util.List;

public class CreateMenuFragment extends Fragment implements CreateMenuElementAdapter.OnButtonClickListener {

        private GameViewModel model;


        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            return inflater.inflate(R.layout.fragment_gamemap_shop_menu, container, false);
        }

        public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
            RecyclerView recyclerView = view.findViewById(R.id.recyclerViewResources);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager;
            layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            CreateMenuElementAdapter adapter = new CreateMenuElementAdapter(new ArrayList<>(), this, getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);

            LiveData<Boolean> isOnCreate = model.getIsOnCreateVisible();
            isOnCreate.observe(this.getViewLifecycleOwner(), bool -> {
                if (bool) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.INVISIBLE);
                }
            });

            LiveData<List<Unit>> possibleUnits = model.getPossibleUnit();
            possibleUnits.observe(this.getViewLifecycleOwner(), units -> {
                ArrayList<CreateMenuElement> list = new ArrayList<>();
                units.forEach(unit -> list.add(new CreateMenuElement(C.getResId(ModelToViewConverterUtils.modelObjectToViewId(unit), R.drawable.class), unit.getNameId(), unit.getCost(), R.string.create_text, this.model.canCreateUnit(unit), this.model.isHeroNotInGame(unit))));
                adapter.setList(list);
            });

            view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
                dismissMenu();
            });
        }

        @Override
        public void onButtonClick(int position, int buttonId) {
            if(buttonId == R.id.createButton){
                this.model.createUnit(position);
                dismissMenu();
            } else if (buttonId == R.id.infoButton){
                this.model.setInfoTitle(this.model.getPossibleUnit().getValue().get(position).getNameId());
                this.model.setInfoDescription(this.model.getPossibleUnit().getValue().get(position).getDescription(), false);
                this.model.setIsOnInfoVisible(true);
            }
        }

        private void dismissMenu(){
            this.model.emptySelectedGameObject();
            this.model.setIsOnCreateVisible(false);
        }
}

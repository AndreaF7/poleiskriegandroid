package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.OnlineLoadGameViewModel;

public class OnlineConfirmDeleteFragment extends Fragment {

    private OnlineLoadGameViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_dialog_title_text_button, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(OnlineLoadGameViewModel.class);
        TextView title = view.findViewById(R.id.dialogTitleText);
        title.setText(R.string.confirm_delete_title);
        TextView msg = view.findViewById(R.id.messageText);
        msg.setText(R.string.confirm_delete_message);
        ButtonWithFeeling button = view.findViewById(R.id.joinButton);
        button.setText(R.string.confirm_delete_button);
        LiveData<Boolean> isConfirmDeleteVisible = model.getIsConfirmDeleteVisible();
        isConfirmDeleteVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            this.model.setIsConfirmDeleteVisible(false);
        });
        view.findViewById(R.id.joinButton).setOnClickListener(v -> {
            this.model.deleteGame();
            this.model.setIsConfirmDeleteVisible(false);
        });
    }
}

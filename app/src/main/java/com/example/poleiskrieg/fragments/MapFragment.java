package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.gridlayout.widget.GridLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.controller.map.VariableCasePart;
import com.example.poleiskrieg.logics.game.util.Coordinates;
import com.example.poleiskrieg.logics.game.util.Pair;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

import java.util.List;
import java.util.Optional;

public class MapFragment extends Fragment{

    GridLayout mapGrid;
    GridLayoutManager layoutManager;
    GameViewModel model;

    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_gamemap_map, container, false);
        mapGrid = view.findViewById(R.id.mapGridLayout);

        return view;
    }



    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        //retrieve info from model
        List<String> terrainsList = model.getTerrainsAsList();
        LiveData<List<VariableCasePart>> variableMapList = model.getActualMapStateAsList();
        Pair<Integer, Integer> dimensions = model.getMapSize();

        mapGrid.setRowCount(dimensions.getValue());
        mapGrid.setColumnCount(dimensions.getKey());
        //initialize grid layout
        for (int i = 0; i < terrainsList.size(); i++) {
            FrameLayout layout = (FrameLayout) LayoutInflater.from(mapGrid.getContext())
                    .inflate(R.layout.layout_case, mapGrid, false);
            ImageView terrainImageView = layout.findViewById(R.id.terrainImageView);
            terrainImageView.setImageResource(C.getResId(terrainsList.get(i), R.drawable.class));
            final int position = i;
            layout.setOnClickListener(v -> {
                final int y = position / dimensions.getKey();
                final int x = position % (dimensions.getKey());
                this.model.cellHit(new Coordinates(x,y));
            });
            //this.setOnClickListener(layout);
            this.mapGrid.addView(layout, i);
        }
        //et observer
        variableMapList.observe(getViewLifecycleOwner(), newList -> {
            for (int i = 0; i < newList.size(); i++) {
                VariableCasePart casePart = newList.get(i);
                FrameLayout layout = (FrameLayout) mapGrid.getChildAt(i);
                ImageView structureImageView = layout.findViewById(R.id.structureImageView);
                ImageView unitImageView = layout.findViewById(R.id.unitImageVIew);
                ImageView borderImageView = layout.findViewById(R.id.borderImageView);

                Optional<String> structure = casePart.getBottom();
                if (structure.isPresent()) {
                    structureImageView.setImageResource(C.getResId(structure.get(), R.drawable.class));
                    unitImageView.setVisibility(View.VISIBLE);
                } else structureImageView.setVisibility(View.INVISIBLE);
                Optional<String> unit = casePart.getTop();
                if (unit.isPresent()) {
                    unitImageView.setImageResource(C.getResId(unit.get(), R.drawable.class));
                    unitImageView.setVisibility(View.VISIBLE);
                } else unitImageView.setVisibility(View.INVISIBLE);
                Optional<String> border = casePart.getBorder();
                if (border.isPresent()) {
                    borderImageView.setImageResource(C.getResId(border.get(), R.drawable.class));
                    borderImageView.setVisibility(View.VISIBLE);
                } else borderImageView.setVisibility(View.INVISIBLE);
            }
        });


    }
}

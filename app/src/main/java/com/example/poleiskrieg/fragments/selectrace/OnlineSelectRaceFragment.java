package com.example.poleiskrieg.fragments.selectrace;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.OnlineGameActivity;
import com.example.poleiskrieg.activities.OnlineLoadGameActivity;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.playerlist.OnlinePlayerListViewModel;
import com.example.poleiskrieg.viewmodel.playerlist.PlayerListViewModelInterface;

public class OnlineSelectRaceFragment extends AbstractSelectRaceFragment {
    @Override
    public PlayerListViewModelInterface getModel() {
        return new ViewModelProvider(requireActivity()).get(OnlinePlayerListViewModel.class);
    }

    public void setSelectButtonListener(View view){
        view.findViewById(R.id.joinButton).setOnClickListener(v -> {
            if(this.model.canAddThePlayer(this.model.getActualPlayerId(), this.model.getPlayerNameEditText().getText().toString(), this.model.getRaceList().get(this.model.getSelectedPosition()).getClass().getSimpleName())){
                if(!this.model.needPlayer()){
                    this.model.createGame();
                }
                Intent intent = new Intent(getActivity(), OnlineLoadGameActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), R.string.invalid_selection, Toast.LENGTH_LONG).show();
                this.model.setIsSelectionMenuVisible(false);
            }
        });
    }
}

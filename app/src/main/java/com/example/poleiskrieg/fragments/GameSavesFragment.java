package com.example.poleiskrieg.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.GameActivity;
import com.example.poleiskrieg.activities.SetupGameActivity;
import com.example.poleiskrieg.adapter.SaveStateElementAdapter;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.LoadGameViewModel;

import java.util.List;
import java.util.Objects;

public class GameSavesFragment extends Fragment implements SaveStateElementAdapter.OnButtonClickListener{

    private LoadGameViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_load_game_saves, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager;
        layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        this.model = new ViewModelProvider(Objects.requireNonNull(getActivity())).get(LoadGameViewModel.class);
        SaveStateElementAdapter adapter = new SaveStateElementAdapter(model.getLocalGames(), this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LiveData<List<GameWithPlayers>> saves = model.getLocalGames();
        saves.observe(this.getViewLifecycleOwner(), bool -> {
            adapter.notifyDataSetChanged();
            if(bool.isEmpty()){
                view.findViewById(R.id.noSaveText).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.noSaveText).setVisibility(View.INVISIBLE);
            }
        });

        view.findViewById(R.id.startNewGame).setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), SetupGameActivity.class);
            v.getContext().startActivity(intent);});
    }

    @Override
    public void onButtonClick(int position, int buttonId) {
        this.model.setPosition(position);
        if (buttonId == R.id.loadButton){
            Intent i = new Intent(getActivity(), GameActivity.class);
            i.putExtra(C.GAME_ID, this.model.getGameId());
            startActivity(i);
        } else if (buttonId == R.id.deleteButton){
            this.model.setIsConfirmDeleteVisible(true);
        } else if (buttonId == R.id.turnButton){
            this.model.setIsInfoVisible(true);
        }
    }
}
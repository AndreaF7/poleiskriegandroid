package com.example.poleiskrieg.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.adapter.ButtonTextImageAdapterLinear;
import com.example.poleiskrieg.element.ButtonTextImage;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

public class OnSelectionMenuFragment extends Fragment implements ButtonTextImageAdapterLinear.OnButtonClickListener{

    private static final int CREATE_BUTTON_POSITION = 0;
    private static final int INFO_BUTTON_POSITION = 1;

    private GameViewModel model;
    private TextView selectedName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gamemap_selected_object_info, container, false);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<ButtonTextImage<Integer>> list = new ArrayList<>();
        list.add(new ButtonTextImage<>(R.drawable.icon_onselection_create, R.string.onselection_menu_create, C.TOP));
        list.add(new ButtonTextImage<>(R.drawable.icon_onselection_info, R.string.onselection_menu_info, C.TOP));

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewResources);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager;
        layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        ButtonTextImageAdapterLinear<Integer> adapter = new ButtonTextImageAdapterLinear<>(list, this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        this.model = new ViewModelProvider(requireActivity()).get(GameViewModel.class);
        LiveData<Optional<GameObject>> selectedGameObject = model.getSelectedGameObject();
        selectedGameObject.observe(this.getViewLifecycleOwner(), gameObject -> {
            if (gameObject.isPresent()) {
                this.selectedName = view.findViewById(R.id.selectedNameText);
                this.selectedName.setText(gameObject.get().getNameId());
                if(this.model.canCreate()){                                     //TODO: VALUTA SE METTERE UN LIVEDATA
                    Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(CREATE_BUTTON_POSITION)).itemView.setVisibility(View.VISIBLE);
                } else {
                    Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(CREATE_BUTTON_POSITION)).itemView.setVisibility(View.INVISIBLE);
                }
            }
        });
        LiveData<Boolean> isVisible = model.getIsOnSelectionVisible();
        isVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onButtonClick(int position) {
        if(position == CREATE_BUTTON_POSITION){
            this.model.setIsOnCreateVisible(true);
        }
        if(position == INFO_BUTTON_POSITION){
            this.model.setInfoTitle(this.model.getSelectedGameObject().getValue().get().getNameId());
            this.model.setInfoDescription(this.model.getSelectedGameObject().getValue().get().getDescription(), true);
            this.model.setIsOnInfoVisible(true);
        }
    }
}

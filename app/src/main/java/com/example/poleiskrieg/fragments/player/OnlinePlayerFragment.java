package com.example.poleiskrieg.fragments.player;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.viewmodel.playerlist.PlayerListViewModelInterface;
import com.example.poleiskrieg.viewmodel.playerlist.OnlinePlayerListViewModel;

public class OnlinePlayerFragment extends AbstractPlayerFragment {
    @Override
    public PlayerListViewModelInterface setViewModel() {
        return new ViewModelProvider(requireActivity()).get(OnlinePlayerListViewModel.class);
    }
}

package com.example.poleiskrieg.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.MainActivity;
import com.example.poleiskrieg.activities.OnlineGameActivity;
import com.example.poleiskrieg.activities.SetupOnlineGameActivity;
import com.example.poleiskrieg.adapter.SaveStateElementAdapter;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.OnlineLoadGameViewModel;

import java.util.List;
import java.util.Objects;

public class OnlineMenuFragment extends Fragment implements SaveStateElementAdapter.OnButtonClickListener{

    private OnlineLoadGameViewModel model;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.online_menu_fragment, container,false);

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                view.getContext().startActivity(intent);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        RecyclerView recyclerView = view.findViewById(R.id.onlineRecyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager;
        layoutManager= new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        this.model = new ViewModelProvider(Objects.requireNonNull(getActivity())).get(OnlineLoadGameViewModel.class);
        SaveStateElementAdapter adapter = new SaveStateElementAdapter(model.getOnlineGames(), this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        LiveData<List<GameWithPlayers>> saves = model.getOnlineGames();
        saves.observe(this.getViewLifecycleOwner(), bool -> {
            adapter.notifyDataSetChanged();
            if(bool.isEmpty()){
                view.findViewById(R.id.noSaveText).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.noSaveText).setVisibility(View.INVISIBLE);
            }
        });

        view.findViewById(R.id.createOnlineGameButton).setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), SetupOnlineGameActivity.class);
            v.getContext().startActivity(intent);});

        view.findViewById(R.id.joinOnlineGameButton).setOnClickListener(v -> {
            //Intent intent = new Intent(v.getContext(), JoinOnlineGameActivity.class);
            //v.getContext().startActivity(intent);
            model.setIsJoinVisible(true);
        });
    }

    @Override
    public void onButtonClick(int position, int buttonId) {
        this.model.setPosition(position);
        if (buttonId == R.id.loadButton){
            if (model.getPlayerIdInGame() != model.getCurrentPlayerId()) {
                Toast.makeText(getContext(), R.string.not_your_turn, Toast.LENGTH_LONG).show();
            } else if (model.isPlayerListFull()) {
                Intent i = new Intent(getActivity(), OnlineGameActivity.class);
                i.putExtra(C.GAME_ID, model.getGameId());
                i.putExtra(C.PLAYER_ID, model.getPlayerIdInGame());
                i.putExtra(C.ONLINE_ID, model.getOnlineId());
                startActivity(i);
                Objects.requireNonNull(getActivity()).finish();
            } else {
                Toast.makeText(getContext(), R.string.game_not_ready, Toast.LENGTH_LONG).show();
            }
        } else if (buttonId == R.id.deleteButton){
            this.model.setIsConfirmDeleteVisible(true);
        } else if (buttonId == R.id.turnButton){
            this.model.setIsInfoVisible(true);
        }
    }
}

package com.example.poleiskrieg.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialFragment extends Fragment {

    private TutorialViewModel model;
    private List<String> infos = new ArrayList<>();
    private List<String> text = new ArrayList<>();
    private List<Integer> images = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tutorial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(TutorialViewModel.class);
        model.getIsTutorialVisible().observe(this.getViewLifecycleOwner(), bool -> {
            view.setVisibility(bool ? View.VISIBLE : View.INVISIBLE);
        });
        view.findViewById(R.id.menuContainer).setOnClickListener(v -> {
            if (!model.getIsTutorialMenuVisible().getValue()) {
                model.setIsTutorialVisible(false);
            }
        });
        view.findViewById(R.id.tutorialEndGameButton).setOnClickListener(v -> {
            model.setIsTutorialMenuVisible(true);
            model.setTutorialTitleText(getString(R.string.end_game));
            this.cleanLists();
            this.infos.add(getString(R.string.win));
            this.infos.add(getString(R.string.lose));
            this.text.add(getString(R.string.win_text));
            this.text.add(getString(R.string.lose_text));
            this.images.add(R.drawable.tutorial_win);
            this.images.add(2);
            this.setStrings();
        });
        view.findViewById(R.id.tutorialTurnButton).setOnClickListener(v -> {
            model.setIsTutorialMenuVisible(true);
            model.setTutorialTitleText(getString(R.string.turns));
            this.cleanLists();
            this.infos.add(getString(R.string.turn_beginning));
            this.infos.add(getString(R.string.turn_playing));
            this.infos.add(getString(R.string.turn_ending));
            this.text.add(getString(R.string.turn_beginning_text));
            this.text.add(getString(R.string.turn_playing_text));
            this.text.add(getString(R.string.turn_ending_text));
            this.images.add(R.drawable.tutorial_capital);
            this.images.add(1);
            this.images.add(2);
            this.setStrings();
        });
        view.findViewById(R.id.tutorialResourcesButton).setOnClickListener(v -> {
            model.setIsTutorialMenuVisible(true);
            model.setTutorialTitleText(getString(R.string.resources));
            this.cleanLists();
            this.infos.add(getString(R.string.get_resources));
            this.infos.add(getString(R.string.resources_limit));
            this.text.add(getString(R.string.get_resources_text));
            this.text.add(getString(R.string.resources_limit_text));
            this.images.add(R.drawable.tutorial_resources);
            this.images.add(1);
            this.setStrings();
        });
        view.findViewById(R.id.tutorialSkilltreeButton).setOnClickListener(v -> {
            model.setIsTutorialMenuVisible(true);
            model.setTutorialTitleText(getString(R.string.skilltree));
            this.cleanLists();
            this.infos.add(getString(R.string.level_structure));
            this.text.add(getString(R.string.level_structure_text));
            this.images.add(R.drawable.tutorial_skilltree);
            this.setStrings();
        });
        view.findViewById(R.id.tutorialStructureButton).setOnClickListener(v -> {
            model.setIsTutorialMenuVisible(true);
            model.setTutorialTitleText(getString(R.string.structures));
            this.cleanLists();
            this.infos.add(getString(R.string.conquering_structures));
            this.infos.add(getString(R.string.resource_producing));
            this.infos.add(getString(R.string.resource_limit));
            this.text.add(getString(R.string.conquering_structures_text));
            this.text.add(getString(R.string.resource_producing_text));
            this.text.add(getString(R.string.resource_limit_text));
            this.images.add(R.drawable.tutorial_structures);
            this.images.add(1);
            this.images.add(2);
            this.setStrings();
        });
        view.findViewById(R.id.tutorialUnitsButton).setOnClickListener(v -> {
            model.setIsTutorialMenuVisible(true);
            model.setTutorialTitleText(getString(R.string.units));
            this.cleanLists();
            this.infos.add(getString(R.string.creation));
            this.infos.add(getString(R.string.movement));
            this.infos.add(getString(R.string.fighting));
            this.infos.add(getString(R.string.vehicles));
            this.text.add(getString(R.string.creation_text));
            this.text.add(getString(R.string.movement_text));
            this.text.add(getString(R.string.fighting_text));
            this.text.add(getString(R.string.vehicles_text));
            this.images.add(R.drawable.tutorial_units_creation);
            this.images.add(R.drawable.tutorial_units);
            this.images.add(R.drawable.tutorial_units_1);
            this.images.add(R.drawable.tutorial_units_vehicles);
            this.setStrings();
        });
    }

    private void setStrings() {
        model.setInfo(infos);
        model.setTexts(text);
        model.setImages(images);
    }

    private void cleanLists() {
        this.infos.clear();
        this.text.clear();
        this.images.clear();
    }
}

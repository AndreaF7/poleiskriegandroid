package com.example.poleiskrieg.fragments.selectrace;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.GameActivity;
import com.example.poleiskrieg.activities.LoadGameActivity;
import com.example.poleiskrieg.activities.OnlineLoadGameActivity;
import com.example.poleiskrieg.activities.PlayersListActivity;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.playerlist.PlayerListViewModelInterface;
import com.example.poleiskrieg.viewmodel.playerlist.PlayersListViewModel;

public class SelectRaceFragment extends AbstractSelectRaceFragment {
    @Override
    public PlayerListViewModelInterface getModel() {
        return new ViewModelProvider(requireActivity()).get(PlayersListViewModel.class);
    }

    public void setSelectButtonListener(View view){
        view.findViewById(R.id.joinButton).setOnClickListener(v -> {
            if(this.model.canAddThePlayer(this.model.getActualPlayerId(), this.model.getPlayerNameEditText().getText().toString(), this.model.getRaceList().get(this.model.getSelectedPosition()).getClass().getSimpleName())){
                if(this.model.needPlayer()){
                    Intent intent = new Intent(getActivity(), PlayersListActivity.class);
                    intent.putExtra(C.GAME_ID, this.model.getGameId());
                    startActivity(intent);
                } else {
                    Intent i = new Intent(getActivity(), GameActivity.class);
                    i.putExtra(C.GAME_ID, this.model.createGame());
                    startActivity(i);
                }
            } else {
                Toast.makeText(getActivity(), R.string.invalid_selection, Toast.LENGTH_LONG).show();
                this.model.setIsSelectionMenuVisible(false);
            }
        });
    }
}

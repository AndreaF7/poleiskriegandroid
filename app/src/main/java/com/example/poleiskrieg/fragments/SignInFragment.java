package com.example.poleiskrieg.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.MainViewModel;

import java.util.Optional;

public class SignInFragment extends Fragment {

    private MainViewModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        LiveData<Boolean> isSignInVisible = model.getIsSignInVisible();
        isSignInVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        view.findViewById(R.id.signMenuContainer).setOnClickListener(v -> {
            this.model.setIsSignInVisible(false);
            this.model.setIsRegisterVisible(false);
        });
        view.findViewById(R.id.openRegisterButton).setOnClickListener(v -> {
            this.model.setIsRegisterVisible(true);
        });
        view.findViewById(R.id.logInButton).setOnClickListener(v -> {
            String username = ((EditText)view.findViewById(R.id.usernameEditText)).getText().toString();
            String password = ((EditText)view.findViewById(R.id.passwordEditText)).getText().toString();
            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(getContext(), R.string.no_username_or_password, Toast.LENGTH_SHORT).show();
            } else {
                this.model.logAccount(username, password).observe(this.getViewLifecycleOwner(), success -> {
                    if (!success) {
                        Toast.makeText(getContext(), R.string.wrong_username_or_password, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.correctly_logged, Toast.LENGTH_SHORT).show();
                        C.setLoggedPLayer(Optional.of(username));
                        this.model.setIsSignInVisible(false);
                        this.model.setIsLoggedIn(true);
                    }
                });
            }
        });
    }
}

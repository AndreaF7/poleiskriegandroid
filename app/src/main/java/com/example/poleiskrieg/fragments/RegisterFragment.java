package com.example.poleiskrieg.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.MainViewModel;

import java.util.Optional;

public class RegisterFragment extends Fragment {

    private MainViewModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        LiveData<Boolean> isRegisterVisible = model.getIsRegisterVisible();
        isRegisterVisible.observe(this.getViewLifecycleOwner(), bool -> {
            if (bool) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.INVISIBLE);
            }
        });
        view.findViewById(R.id.registerMenuContainer).setOnClickListener(v -> {
            this.model.setIsRegisterVisible(false);
        });
        view.findViewById(R.id.registerButton).setOnClickListener(v -> {
            String username = ((EditText)view.findViewById(R.id.registerUsernameEditText)).getText().toString();
            String password = ((EditText)view.findViewById(R.id.registerPasswordEditText)).getText().toString();
            String confirm = ((EditText)view.findViewById(R.id.confirmPasswordEditText)).getText().toString();
            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(getContext(), R.string.no_username_or_password, Toast.LENGTH_SHORT).show();
            } else if (!password.equals(confirm)) {
                Toast.makeText(getContext(), R.string.passwords_no_match, Toast.LENGTH_SHORT).show();
            } else {
                this.model.registerAccount(username, password).observe(this.getViewLifecycleOwner(), success -> {
                    if (!success) {
                        Toast.makeText(getContext(), R.string.username_already_present, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.correctly_registered, Toast.LENGTH_SHORT).show();
                        C.setLoggedPLayer(Optional.of(username));
                        this.model.setIsRegisterVisible(false);
                        this.model.setIsSignInVisible(false);
                        this.model.setIsLoggedIn(true);
                    }
                });
            }
        });
    }
}

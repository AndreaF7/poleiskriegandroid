package com.example.poleiskrieg.element;

import com.example.poleiskrieg.logics.game.model.Cost;

public class CreateMenuElement {
    private int image;
    private int nameText;
    private Cost cost;
    private int actionText;
    private boolean canDoAction;
    private boolean isHeroNotInGame;


    public CreateMenuElement(int image, int nameText, Cost cost, int actionText, boolean canCreate, boolean isHeroNotInGame) {
        this.image = image;
        this.nameText = nameText;
        this.cost = cost;
        this.actionText = actionText;
        this.canDoAction = canCreate;
        this.isHeroNotInGame = isHeroNotInGame;
    }

    public CreateMenuElement(int image, int nameText, Cost cost, int actionText, boolean canCreate) {
        this.image = image;
        this.nameText = nameText;
        this.cost = cost;
        this.actionText = actionText;
        this.canDoAction = canCreate;
        this.isHeroNotInGame = true;
    }

    public int getImageResource() {
        return this.image;
    }

    public int getNameTextResource() {
        return this.nameText;
    }

    public Cost getCostResource() {
        return this.cost;
    }

    public int getActionText() {
        return this.actionText;
    }

    public boolean getCanCreateResource(){
        return this.canDoAction;
    }

    public boolean getIsHeroNotInGameResource(){
        return this.isHeroNotInGame;
    }
}

package com.example.poleiskrieg.element;
public class ButtonTextImage<X> {
    private int image;
    private X textId;
    private int imageDirection; //0 = left, 1 = top, 2 = right, 3 = bottom

    public ButtonTextImage(int image, X textId, int imageDirection) {
        this.image = image;
        this.textId = textId;
        this.imageDirection = imageDirection;
    }

    public int getImageResource() {
        return this.image;
    }

    public X getTextResource() {
        return textId;
    }

    public int getImageDirectionResources(){
        return this.imageDirection;
    }
}

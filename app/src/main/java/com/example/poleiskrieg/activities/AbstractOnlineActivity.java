package com.example.poleiskrieg.activities;

import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.poleiskrieg.network.NetworkCallbacksFactory;
import com.example.poleiskrieg.network.NetworkManager;

public abstract class AbstractOnlineActivity extends AppCompatActivity {

    private ConnectivityManager.NetworkCallback connectionLostCallback;
    private NetworkManager networkManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkManager = NetworkManager.getInstance(getApplicationContext());
        NetworkCallbacksFactory networkCallbacksFactory = new NetworkCallbacksFactory(this);
        connectionLostCallback = networkCallbacksFactory.getShowDialogOnConnectionLost();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!networkManager.isNetworkConnectionAvailable()){
            connectionLostCallback.onUnavailable();
        }
        networkManager.registerNetworkCallBack(connectionLostCallback);
    }

    @Override
    protected void onStop() {
        super.onStop();
        networkManager.unregisterCallback(connectionLostCallback);
    }
}

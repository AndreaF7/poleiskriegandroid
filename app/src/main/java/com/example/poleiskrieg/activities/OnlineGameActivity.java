package com.example.poleiskrieg.activities;

import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.repositories.game.OnlineGameRepository;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;
import com.example.poleiskrieg.viewmodel.Factories.GameViewModelFactory;
import com.example.poleiskrieg.viewmodel.Factories.OnlineGameViewModelFactory;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;
import com.example.poleiskrieg.viewmodel.gameviewmodel.GameViewModel;

public class OnlineGameActivity extends AbstractOnlineActivity {

    private GameViewModel model;
    private long gameId;
    private long onlineId;
    private int playerId;
    private OnlineGameRepository gameRepository;
    private TutorialViewModel tutorialViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameId = getIntent().getLongExtra(C.GAME_ID, -1);
        onlineId = getIntent().getLongExtra(C.ONLINE_ID, -1);
        playerId = getIntent().getIntExtra(C.PLAYER_ID, -1);
        Log.d(C.DEBUG_TAG, "" + gameId);
        Log.d(C.DEBUG_TAG, "" + onlineId);
        Log.d(C.DEBUG_TAG, "" + playerId);
        if (playerId == -1) {
            throw new IllegalArgumentException();
        }
        ClassScan.setConfigurationFile(R.raw.scan_classes, this);
        gameRepository = new OnlineGameRepository(getApplication(), gameId, onlineId, playerId);
        model = new ViewModelProvider(this, new OnlineGameViewModelFactory(getApplication(), gameRepository, playerId)).get(GameViewModel.class);
        tutorialViewModel = new ViewModelProvider(this).get(TutorialViewModel.class);
        setContentView(R.layout.activity_game);

        //model = new ViewModelProvider(this, new OnlineGameViewModelFactory(getApplication(), gameRepository)).get(GameViewModel.class);
        /*gameRepository.isInitialized().observe(this, initialized -> {
            if (initialized){
                model = new ViewModelProvider(this, new OnlineGameViewModelFactory(getApplication(), gameRepository)).get(GameViewModel.class);
                model.setPlayerId(playerId);

            }
            //TODO ci possono essere problemi causati dal fatto che games cancellati in menu princiipale
        });
        gameRepository.init();*/
    }

    @Override
    public void onBackPressed() {
        if (tutorialViewModel.getIsTutorialMenuVisible().getValue()) {
            tutorialViewModel.setIsTutorialMenuVisible(false);
        } else {
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}

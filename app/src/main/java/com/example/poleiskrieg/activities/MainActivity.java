package com.example.poleiskrieg.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.util.Log;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.database.AppDatabase;
import com.example.poleiskrieg.database.tables.AccountEntity;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.viewmodel.MainViewModel;
import com.example.poleiskrieg.viewmodel.TutorialViewModel;

import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class MainActivity extends AppCompatActivity {

    private MainViewModel model;
    private TutorialViewModel tutorialViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //startService(new Intent(MainActivity.this, SoundService.class));
        Future<AccountEntity> future;
        AccountEntity result = null;
        try {
            future = AppDatabase.databaseWriteExecutor.submit(() -> AppDatabase.getDatabase(getApplicationContext()).getAccountDao().getLoggedPlayer());
            result = future.get();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        C.setLoggedPLayer(result == null ? Optional.empty() : Optional.of(result.username));
        setContentView(R.layout.activity_main);
        this.model = new ViewModelProvider(this).get(MainViewModel.class);
        this.tutorialViewModel = new ViewModelProvider(this).get(TutorialViewModel.class);
    }

    @Override
    public void onBackPressed() {
        if(tutorialViewModel.getIsTutorialMenuVisible().getValue().booleanValue()) {
            tutorialViewModel.setIsTutorialMenuVisible(false);
        } else if(model.getIsCreditsVisible().getValue()) {
            model.setIsCreditsVisible(false);
        } else if(model.getIsSettingsVisible().getValue() || tutorialViewModel.getIsTutorialVisible().getValue()) {
            model.setIsSettingsVisible(false);
            tutorialViewModel.setIsTutorialVisible(false);
        } else if(model.getIsSignInVisible().getValue() || model.getIsRegisterVisible().getValue()) {
            model.setIsSignInVisible(false);
            model.setIsRegisterVisible(false);
        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //stopService(new Intent(MainActivity.this, SoundService.class));
    }

}

package com.example.poleiskrieg.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.viewmodel.loadgameviewmodel.OnlineLoadGameViewModel;

import java.util.Objects;

public class OnlineMenuActivity extends AppCompatActivity {

    private OnlineLoadGameViewModel model;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_menu_activity);
        this.model = new ViewModelProvider(this).get(OnlineLoadGameViewModel.class);
    }

    @Override
    public void onBackPressed() {
        finish();
        model.setIsJoinVisible(false);
    }
}

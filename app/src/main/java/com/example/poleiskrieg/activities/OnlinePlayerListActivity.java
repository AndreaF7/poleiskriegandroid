package com.example.poleiskrieg.activities;

import android.os.Bundle;

import androidx.lifecycle.ViewModelProvider;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.util.C;
import com.example.poleiskrieg.util.ClassScan;
import com.example.poleiskrieg.viewmodel.Factories.OnlinePlayerListViewModelFactory;
import com.example.poleiskrieg.viewmodel.playerlist.OnlinePlayerListViewModel;

public class OnlinePlayerListActivity extends AbstractOnlineActivity{

    private OnlinePlayerListViewModel model;
    private long gameId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.gameId = getIntent().getLongExtra(C.GAME_ID, -1);
        setContentView(R.layout.online_activity_players_list);
        ClassScan.setConfigurationFile(R.raw.scan_classes, this);

        this.model = new ViewModelProvider(this, new OnlinePlayerListViewModelFactory(getApplication(), gameId)).get(OnlinePlayerListViewModel.class);
    }

}

package com.example.poleiskrieg.logics.game.model.objects.structures;

/**
 * The City interface is a Marker interface, in fact it hasn't declaration of
 * some methods. It is use to mark a particular kind of Structure.
 */
public interface City extends ResourceProducer {

}

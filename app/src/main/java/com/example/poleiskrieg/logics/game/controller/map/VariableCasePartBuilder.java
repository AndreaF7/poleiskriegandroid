package com.example.poleiskrieg.logics.game.controller.map;

import androidx.annotation.Nullable;

import java.util.Optional;

import com.example.poleiskrieg.logics.game.util.mapbuilder.AbstractCaseBuilder;

/**
 * 
 * An implementation for the interface VariableCasePart.
 *
 */
public class VariableCasePartBuilder extends AbstractCaseBuilder<VariableCasePart> {

    private boolean built;

    /**{@inheritDoc}**/@Override
    public VariableCasePart build() {
        if (this.built) {
            throw new IllegalStateException();
        }
        this.built = true;
        return new VariableCasePart() {

            /**{@inheritDoc}**/@Override
            public Optional<String> getTop() {
                return VariableCasePartBuilder.super.getTop();
            }

            /**{@inheritDoc}**/@Override
            public Optional<String> getBottom() {
                return VariableCasePartBuilder.super.getBottom();
            }

            /**{@inheritDoc}**/@Override
            public Optional<String> getBorder() {
                return VariableCasePartBuilder.super.getBorder();
            }

            @Override
            public int hashCode() {
                int hash = 5;
                hash = 89  * hash + (this.getBottom().isPresent() ? this.getBottom().get().hashCode() : 0);
                hash = 89  * hash + (this.getTop().isPresent() ? this.getTop().get().hashCode() ^ (this.getTop().get().hashCode() >>> 32) : 0);
                hash = 89  * hash + (this.getBorder().isPresent() ? this.getBorder().get().hashCode() * this.getBorder().hashCode() : 0);
                return hash;
            }

            @Override
            public boolean equals(@Nullable Object obj) {
                if (obj == this) {
                    return true;
                }
                if (!(obj instanceof VariableCasePart)) {
                    return false;
                }
                VariableCasePart c = (VariableCasePart) obj;
                return this.getBottom().equals(c.getBottom()) && this.getTop().equals(c.getTop()) && this.getBorder().equals(c.getBorder());
            }
        };
    }

}

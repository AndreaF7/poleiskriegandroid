package com.example.poleiskrieg.logics.game.view;

import com.example.poleiskrieg.logics.game.controller.Updater;

/**
 * a marker interface.
 *
 */
public interface UpdatableView extends Updater {

}

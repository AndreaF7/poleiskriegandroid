package com.example.poleiskrieg.logics.game.model.objects;

import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Map;
import java.util.Optional;

/**
 * Describes an object on the game map. create class ownable game object, since
 * terrain doesn't have owner.
 */
public interface GameObject {

    /**
     * 
     * @return the owner of the object.
     */
    Optional<Player> getOwner();

    /**
     * sets the owner of the object.
     * 
     * @param player indicate if the object has an owner or not.
     */
    void set(Optional<Player> player);

    /**
     * sets the owner of the object as player.
     * 
     * @param player the new owner.
     */
    void setOwner(Player player);

    /**
     * removes the owner of the object.
     */
    void removeOwner();

    /**
     * @return game object name id.
     */
    int getNameId();

    /**
     * @return game object description.
     */
    Map<Integer, Optional<String>> getDescription();

    /**
     * 
     * @returnreturn owner's name. If it doesn't exist, it return the neutral owner.
     */
    String getOwnerName();
}

package com.example.poleiskrieg.logics.game.model.objectives;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.map.ObservableGameMap;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.util.Coordinates;

import java.util.Optional;

/**
 * This is the standard Objective that is winning if the player is the last one
 * alive.
 */
public class LastStandingObjective extends AbstractObjective {

    private static final int DESCRIPTION = R.string.objective_last_standing;

    /**
     * Initialize the class.
     */
    public LastStandingObjective() {
        super(DESCRIPTION);
    }

    /** {@inheritDoc} **/
    @Override
    public boolean isCompleted(final ObservableGameMap actualGameMap, final Player player) {
        Optional<Unit> unit;
        Optional<Structure> structure;
        for (int i = 0; i < actualGameMap.getMapSize().getKey(); i++) {
            for (int j = 0; j < actualGameMap.getMapSize().getValue(); j++) {
                unit = actualGameMap.getUnit(new Coordinates(i, j));
                structure = actualGameMap.getStructure(new Coordinates(i, j));
                if (unit.isPresent() && unit.get().getOwner().isPresent()
                        && !unit.get().getOwner().get().equals(player)) {
                    return false;
                }
                if (structure.isPresent() && structure.get().getOwner().isPresent()
                        && !structure.get().getOwner().get().equals(player)) {
                    return false;
                }
            }
        }
        return true;
    }

}

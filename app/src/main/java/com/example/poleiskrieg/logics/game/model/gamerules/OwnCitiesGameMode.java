package com.example.poleiskrieg.logics.game.model.gamerules;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.objectives.CitiesOwnedObjective;
import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.util.Pair;

/**
 * Game Mode that ends with a number of cities owned by a Player.
 */
public class OwnCitiesGameMode implements GameRules {

    private static final int DESCRIPTION = R.string.gamemode_owncities_name;
    private static final int LONG_DESCRIPTION = R.string.gamemode_owncities_description;
    private static final OwnCitiesGameMode SINGLETON = new OwnCitiesGameMode();

    private static final int INITIAL_VALUES = 250;

    private final Pair<Integer, Integer> mapSize = new Pair<Integer, Integer>(20, 20);

    /**
     * @return the singleton
     */
    public static OwnCitiesGameMode get() {
        return SINGLETON;
    }

    /** {@inheritDoc} **/
    @Override
    public Pair<Integer, Integer> getMapSize() {
        return this.mapSize;
    }

    /** {@inheritDoc} **/
    @Override
    public Objective generateObjective() {
        return new CitiesOwnedObjective();
    }

    /** {@inheritDoc} **/
    @Override
    public int getInitialValues() {
        return INITIAL_VALUES;
    }

    /** @inheritDoc} **/
    @Override
    public int getName() {
        return DESCRIPTION;
    }

    /** @inheritDoc} **/
    @Override
    public int getDescription() {
        return LONG_DESCRIPTION;
    }

}

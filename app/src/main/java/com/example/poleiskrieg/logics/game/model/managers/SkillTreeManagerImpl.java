package com.example.poleiskrieg.logics.game.model.managers;

import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTree;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeImpl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * The SkillTreeManagerImpl is a class that implements SkillTreeManager. It has
 * methods to get all the attributes of a Skilltree, to get the attributes that
 * can be updated and to upgrade a certain attribute of a given Skilltree.
 *
 */
public class SkillTreeManagerImpl implements SkillTreeManager {

    private final Map<Player, SkillTree> skilltrees;

    /**
     * Constructor.
     * 
     * @param players is a list of all players in the actual match.
     */
    public SkillTreeManagerImpl(final List<Player> players) {
        this.skilltrees = new LinkedHashMap<>();
        players.forEach(p -> {
            skilltrees.put(p, new SkillTreeImpl());
        });
    }

    /**
     * Constructor used to load a game
     * 
     * @param skilltrees
     */
    public SkillTreeManagerImpl(final Map<Player, SkillTree> skilltrees) {
        this.skilltrees = skilltrees;
    }

    private void verifyPlayer(final Player player) {
        if (!skilltrees.containsKey(player)) {
            throw new IllegalStateException();
        }
    }

    /** {@inheritDoc} **/
    @Override
    public List<SkillTreeAttribute> getAllPlayerAttributes(final Player player) {
        verifyPlayer(player);
        return (skilltrees.get(player).getAllAttributes());

    }

    /** {@inheritDoc} **/
    @Override
    public List<SkillTreeAttribute> getUpgradeblePlayerAttributes(final Player player) {
        verifyPlayer(player);
        return (skilltrees.get(player).getUpgradebleAttributes());
    }

    /** {@inheritDoc} **/
    @Override
    public void upgradePlayerAttribute(final Player player, final SkillTreeAttribute attribute) {
        skilltrees.get(player).upgradeAttribute(attribute);
    }

}

package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.util.ClassScan;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The SkillTreeImpl is a class that implements the SkillTree interface, so it
 * implements methods for update or get the attributes SkillTreeAttribute. This
 * class use some ClassGraph methods for manage the attributes.
 */
public class SkillTreeImpl implements SkillTree {

    private final List<SkillTreeAttribute> attributes;

    /**
     * SkillTreeImpl constructor.
     */
    public SkillTreeImpl() {
        this.attributes = getAttributes();
    }

    public SkillTreeImpl(List<SkillTreeAttribute> attributes){
        this.attributes = attributes;
    }

    private List<SkillTreeAttribute> getAttributes() {
        List<SkillTreeAttribute> attributes = new LinkedList<>();
        List<String> string = ClassScan.get().getSkilltreeAttributeClasses();
        ClassScan.get().getSkilltreeAttributeClasses().forEach(s -> {
            try {
                if (!Modifier.isAbstract(Class.forName(s).getModifiers())) {
                    attributes.add((SkillTreeAttribute) Class.forName(s).getConstructor().newInstance());
                }
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException | NoSuchMethodException | SecurityException
                    | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        return attributes;
    }

    /** {@inheritDoc} **/
    @Override
    public List<SkillTreeAttribute> getAllAttributes() {
        return this.attributes;
    }

    /** {@inheritDoc} **/
    @Override
    public List<SkillTreeAttribute> getUpgradebleAttributes() {
        return this.attributes.stream().filter(attribute -> attribute.canUpgrade()).collect(Collectors.toList());
        }

    /** {@inheritDoc} **/
    @Override
    public void upgradeAttribute(final SkillTreeAttribute attribute) {
        final List<SkillTreeAttribute> upgradeble = getUpgradebleAttributes();
        if (upgradeble.contains(attribute)) {
            upgradeble.get(upgradeble.indexOf(attribute)).upgrade();
        }
    }

}

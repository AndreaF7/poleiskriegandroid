package com.example.poleiskrieg.logics.game.model.objects.structures;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.objects.AbstractGameObject;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Vehicle;
import com.example.poleiskrieg.logics.game.model.skilltree.ShipLevel;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The VehicleProducer that produces ships.
 */
public class Harbor extends AbstractGameObject implements VehicleProducer {

    private static final String TERRAIN = "Water";
    private SkillTreeManager skillTreeManager;

    /**
     * @param skillTreeManager the skilltreemanager for this harbor to create the correct lvl ship
     */
    public Harbor(final SkillTreeManager skillTreeManager) {
        this.skillTreeManager = skillTreeManager;
    }

    public Harbor(){}

    /** {@inheritDoc} **/
    @Override
    public Vehicle getVehicle(final Unit unit) {
        if(this.skillTreeManager == null){
            throw new IllegalStateException();
        }
        final List<SkillTreeAttribute> attributes = this.skillTreeManager.getAllPlayerAttributes(unit.getOwner().get());
        if (attributes.stream().filter(a -> a instanceof ShipLevel).findFirst().isPresent()) {
            return ((ShipLevel) (attributes.stream().filter(a -> a instanceof ShipLevel).findFirst().get()))
                    .getActualShip(unit);
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    public void setSkillTreeManager(SkillTreeManager skillTreeManager) {
        this.skillTreeManager = skillTreeManager;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean canBeBuilt(final Terrain terrain) {
        return terrain.getId().equals(TERRAIN);
    }

    @Override
    public int getNameId() {
        return R.string.structure_harbor;
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getDescription() {
        Map<Integer, Optional<String>> descriptionMap = new LinkedHashMap<>();
        descriptionMap.put(R.string.game_object_info_owner, Optional.of(getOwnerName()));
        return descriptionMap;
    }
}

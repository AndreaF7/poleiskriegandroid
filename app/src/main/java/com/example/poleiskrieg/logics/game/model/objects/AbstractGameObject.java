package com.example.poleiskrieg.logics.game.model.objects;

import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.util.C;

import java.util.Optional;

/**
 * implements the way the object id is retrieved. (NOTA: non ha metodi astratti)
 */
public abstract class AbstractGameObject implements GameObject {

    private Optional<Player> owner = Optional.empty();

    /** {@inheritDoc} **/
    @Override
    public Optional<Player> getOwner() {
        return this.owner;
    }

    /** {@inheritDoc} **/
    @Override
    public void set(final Optional<Player> player) {
        this.owner = player;
    }

    /** {@inheritDoc} **/
    @Override
    public void setOwner(final Player player) {
        set(Optional.of(player));
    }

    /** {@inheritDoc} **/
    @Override
    public void removeOwner() {
        set(Optional.empty());
    }

    /** {@inheritDoc} **/
    @Override
    public String getOwnerName() {
        return this.owner.isPresent() ? owner.get().getName() : C.NEUTRAL;
    }

}

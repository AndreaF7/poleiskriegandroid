package com.example.poleiskrieg.logics.game.model.objects.structures;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.BasicCostImpl;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Optional;

/**
 * The Level1Capital class extends CapitalImpl and represent the first level of a Capital. 
 */
public class Level1Capital extends CapitalImpl {

    private static final double ENEMY_ATTACK_REDUCTION = 0.80;
    private static final int POPULATION_BOOST = 5;
    private static final int NAME_ID = R.string.structure_capital_level1;

    /**
     * Level1Capital constructor.
     * @param player is the player that own the capital.
     */
    public Level1Capital(final Optional<Player> player) {
        super(player, NAME_ID, ENEMY_ATTACK_REDUCTION, POPULATION_BOOST,
                new BasicCostImpl());
    }

    public Level1Capital(){this(Optional.empty());}
}

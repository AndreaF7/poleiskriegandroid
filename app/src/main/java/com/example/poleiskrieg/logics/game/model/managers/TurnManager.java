package com.example.poleiskrieg.logics.game.model.managers;

import com.example.poleiskrieg.logics.game.model.map.ObservableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.List;
import java.util.Optional;

/**
 * Handle the turns of the match.
 */
public interface TurnManager {

    /**
     * Initiate a new turn for the next player.
     */
    void nextTurn();

    /**
     * @param map the map to be passed to the objective's checks
     * 
     * @return the winner player if it has win
     */
    Optional<Player> getWinner(ObservableGameMap map);

    /**
     * @return the player actually going through his turn
     */
    Player getTurnPlayer();

    /**
     * @param player the player that has lost
     */
    void removePlayer(Player player);

    List<Player> getInGamePLayers();

}

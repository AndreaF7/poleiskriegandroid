package com.example.poleiskrieg.logics.game.model.objects.terrains;

import com.example.poleiskrieg.logics.game.model.abilities.Ability;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;

import java.util.Set;

/**
 * A terrain can have on it a structure or nothing. In some terrains structures
 * can't be built.
 */
public interface Terrain extends GameObject {

    /**
     * @return the abilities required to step on this terrain
     */
    Set<Ability> getRequiredAbilities();

    /**
     * @return the unique name identifier of this terrain
     */
    String getId();

}

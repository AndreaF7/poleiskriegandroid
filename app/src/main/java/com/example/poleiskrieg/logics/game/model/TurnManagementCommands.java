package com.example.poleiskrieg.logics.game.model;

import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Optional;

/**
 * commands regarding the turn management.
 */
public interface TurnManagementCommands {
    /**
     * Initiate a new turn changing the current player.
     */
    void nextTurn();

    /**
     * @return the winner player if any.
     */
    Optional<Player> getWinnerPlayer();

    // ------------------------------------------------------------------------------

    /**
     * @return the current player.
     */
    Player getCurrentPlayer();

    /**
     * @return a string representation of the player.
     */
    String getPlayerInfo();

    /**
     *
     * @return true if this is the last turn.
     */
    boolean isNextTurnWin();

    /**
     * delete the current game.
     */
    void deleteGame();
}

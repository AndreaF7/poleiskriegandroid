package com.example.poleiskrieg.logics.game.model.map;

import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;

/**
 *package protected.
 */
class CaseFactoryImpl implements CaseFactory {

    /**{@inheritDoc}**/@Override
    public Case getEmptyCase(final Terrain caseTerrain) {
        return new CaseImpl(caseTerrain);
    }

}

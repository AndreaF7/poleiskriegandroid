package com.example.poleiskrieg.logics.game.model.gamerules;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.objectives.LastStandingObjective;
import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.util.Pair;

/**
 * A game mode in which each player starts with much more resources than normal.
 */
public class BigStartGameMode implements GameRules {

    private static final int DESCRIPTION = R.string.gamemode_bigstart_name;
    private static final int LONG_DESCRIPTION = R.string.gamemode_bigstart_description;
    private static final BigStartGameMode SINGLETON = new BigStartGameMode();

    private static final int INITIAL_VALUES = 3000;

    private final Pair<Integer, Integer> mapSize = new Pair<Integer, Integer>(20, 20);

    /**
     * @return the singleton
     */
    public static BigStartGameMode get() {
        return SINGLETON;
    }

    /** {@inheritDoc} **/
    @Override
    public Pair<Integer, Integer> getMapSize() {
        return this.mapSize;
    }

    /** {@inheritDoc} **/
    @Override
    public Objective generateObjective() {
        return new LastStandingObjective();
    }

    /** {@inheritDoc} **/
    @Override
    public int getInitialValues() {
        return INITIAL_VALUES;
    }

    /** @inheritDoc} **/
    @Override
    public int getName() {
        return DESCRIPTION;
    }

    @Override
    public int getDescription() {
        return LONG_DESCRIPTION;
    }
}

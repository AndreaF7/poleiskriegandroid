package com.example.poleiskrieg.logics.game.model.managers;

import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.resources.BasicResources;
import com.example.poleiskrieg.logics.game.model.resources.Resource;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * The ResourceManager for the Basic Resources.
 */
public class BasicResourceManager extends AbstractResourceManager {

    /**
     * @param players       the players to be managed
     * 
     * @param initialValues the initial values of the resources
     */
    public BasicResourceManager(final List<Player> players, final int initialValues) {
        super(players, initialValues);
    }

    /**
     * used to load saved data
     * @param globalResources
     * @param maxResources
     */
    public BasicResourceManager(final Map<Player, Map<Resource, Integer>> globalResources, final Map<Player, Map<Resource, Integer>> maxResources){
        super(globalResources, maxResources);
    }

    /** @{inheritDoc} **/
    @Override
    protected Map<Resource, Integer> getResource(final int value) {
        final Map<Resource, Integer> resourcesMap = new LinkedHashMap<>();
        for (final Resource r : BasicResources.values()) {
            if (r.getNameId().equals("Population")) {
                resourcesMap.put(r, 0);
            } else {
                resourcesMap.put(r, value);
            }
        }
        return resourcesMap;
    }

    /** @{inheritDoc} **/
    @Override
    public void decreaseMax(final Player player, final int quantity) {
        for (final Entry<Resource, Integer> e : this.getMaxResources(player).entrySet()) {
            this.setMaxResourceQuantity(player, e.getKey(), e.getValue() - quantity * e.getKey().getModifier().orElse(1));
            if (!e.getKey().getNameId().equals("Population") && this.getGlobalResources(player).get(e.getKey()) > this.getMaxResources(player).get(e.getKey())) {
                this.setGlobalResourceQuantity(player, e.getKey(), e.getValue());
            }
        }
    }

}

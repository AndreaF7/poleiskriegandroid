package com.example.poleiskrieg.logics.game.model.skilltree;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.BasicCostImpl;
import com.example.poleiskrieg.logics.game.model.Cost;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The WoodGoldBoost class extends SkillTreeAttributeAbstract. If the user
 * increase the level of this class, he will get more gold and wood from his
 * structure at the beginning of his turn.
 */
public class WoodGoldBoost extends SkillTreeAttributeAbstract {

    private static final int ATTRIBUTE_DESCRIPTION = R.string.skilltree_attribute_wood_gold_description;
    private static final int ATTRIBUTE_NAME = R.string.skilltree_attribute_wood_gold;
    private static final int INITIAL_VALUE = 0;
    private static final int COST_VALUE = 200;
    private final List<Double> boost = Arrays.asList(1.0, 1.25, 1.50, 1.75, 2.00);
    private final List<Cost> costList;

    /**
     * WoodGoldBoost constructor.
     */
    public WoodGoldBoost() {
        super(INITIAL_VALUE);
        this.costList = createCostList();
    }

    private List<Cost> createCostList() {
        final List<Cost> costList = new LinkedList<>();
        boost.forEach(d -> {
            if (d != 0) {
                costList.add(new BasicCostImpl(
                        Optional.of((int) Math.round((COST_VALUE + (100 * d)) * ((boost.indexOf(d) + 1) / 2))),
                        Optional.of((int) Math.round((COST_VALUE + (100 * d)) * ((boost.indexOf(d) + 1) / 2))),
                        Optional.empty()));
            } else {
                costList.add(new BasicCostImpl());
            }
        });
        return costList;
    }

    private int getPercentualValue(final Double value) {
        return (int) Math.round((value - 1) * 100);
    }

    @Override
    public int getAttributeNameId() {
        return ATTRIBUTE_NAME;
    }

    @Override
    public int getAttributeLevelNameId() {
        switch(getCurrentValue() + 1) {
            case 1:
                return R.string.skilltree_attribute_wood_gold_level_1;
            case 2:
                return R.string.skilltree_attribute_wood_gold_level_2;
            case 3:
                return R.string.skilltree_attribute_wood_gold_level_3;
            case 4:
                return R.string.skilltree_attribute_wood_gold_level_4;
            case 5:
                return R.string.skilltree_attribute_wood_gold_level_5;
            default:
                return R.string.skilltree_attribute_wood_gold;
        }
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getAttributeDescription(Optional<Player> owner) {
        Map<Integer, Optional<String>> description = new LinkedHashMap<>();
        description.put(ATTRIBUTE_DESCRIPTION, Optional.of(getPercentualValue(boost.get(getCurrentValue()+1)) + "%"));
        return description;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean canUpgrade() {
        return getCurrentValue() < boost.size() - 1;
    }

    /** {@inheritDoc} **/
    @Override
    public Cost getCost() {
        return this.costList.get(getCurrentValue() + 1);
    }

    /**
     * This method can be use to get the actual resource boost.
     * @return the resource boost.
     */
    public double getBoost() {
        return this.boost.get(getCurrentValue());
    }

}

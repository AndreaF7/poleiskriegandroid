package com.example.poleiskrieg.logics.game.model.map;

import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.util.Coordinates;
import com.example.poleiskrieg.logics.game.util.Pair;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A game map that can't be modified but can be read.
 */
public interface ObservableGameMap {

    /**
     * 
     * @return the size of the game map (width,height)
     */
    Pair<Integer, Integer> getMapSize();

    /**
     * @param cords the cords of the case to be checked for the terrain
     * 
     * @return the terrain of the case
     */
    Terrain getTerrain(Coordinates cords);

    /**
     * @param cords the cords of the case to be checked for the structure
     * 
     * @return the structure associated with this terrain if present, Optional.Empty
     *         otherwise
     */
    Optional<Structure> getStructure(Coordinates cords);

    /**
     * @param cords the cords to be checked for the pg
     * 
     * @return the Unit on the case if present
     */
    Optional<Unit> getUnit(Coordinates cords);

    /**
     * 
     * @return the the actual updated state of the game map
     */
    Map<Coordinates, List<GameObject>> toMap();
}

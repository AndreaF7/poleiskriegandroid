package com.example.poleiskrieg.logics.game.model.gamerules;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.objectives.LastStandingObjective;
import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.util.Pair;

//TODO IF SIGNLETON SET CONSTRUCTOR PRIVATE
/**
 * The default game mode that is: each player starts from his own capital with
 * one soldier and wins if he's the only player left on the map.
 */
public class LastAliveGameMode implements GameRules {

    private static final int DESCRIPTION = R.string.gamemode_lastalive_name;
    private static final int LONG_DESCRIPTION = R.string.gamemode_lastalive_description;
    private static final LastAliveGameMode SINGLETON = new LastAliveGameMode();

    private static final int INITIAL_VALUES = 250;

    private final Pair<Integer, Integer> mapSize = new Pair<Integer, Integer>(20, 20);

    /**
     * @return the singleton
     */
    public static LastAliveGameMode get() {
        return SINGLETON;
    }

    /** {@inheritDoc} **/
    @Override
    public Pair<Integer, Integer> getMapSize() {
        return this.mapSize;
    }

    /** {@inheritDoc} **/
    @Override
    public Objective generateObjective() {
        return new LastStandingObjective();
    }

    /** {@inheritDoc} **/
    @Override
    public int getInitialValues() {
        return INITIAL_VALUES;
    }

    /** @inheritDoc} **/
    @Override
    public int getName() {
        return DESCRIPTION;
    }

    /** @inheritDoc} **/
    @Override
    public int getDescription() {
        return LONG_DESCRIPTION;
    }

}

package com.example.poleiskrieg.logics.game.model.objects.structures;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.resources.BasicResources;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Produces Wood.
 */
public class Carpentry extends AbstractResourceProducer {

    private static final String TERRAIN = "Base";
    private static final int INITIAL_VALUE = 5000;
    private static final int PRODUCED_VALUE = 200;

    private final int total;
    private int produced;
    private int left;

    /**
     * Initialize the Structure.
     */
    public Carpentry() {
        super(BasicResources.WOOD);
        final List<String> correctTerrains;
        correctTerrains = new LinkedList<>();
        correctTerrains.add(TERRAIN);
        super.addBuildableTerrain(correctTerrains);
        this.total = INITIAL_VALUE;
        this.left = this.total;
    }

    /** {@inheritDoc} **/
    @Override
    public int produce(final double modifier) {
        super.checkOwner();
        produced = (int) (PRODUCED_VALUE * modifier);
        return produced;
    }

    @Override
    public void setProduced(int value) {
        this.produced = value;
    }

    /** {@inheritDoc} **/
    @Override
    public int getProducedQuantity() {
        if (this.left - this.produced < 0) {
            this.produced = this.left;
            this.left = 0;
            return this.produced;
        }
        this.left -= this.produced;
        return this.produced;
    }

    /** {@inheritDoc} **/
    @Override
    public boolean isOver() {
        return this.left == 0;
    }

    @Override
    public int getNameId() {
        return R.string.structure_carpentry;
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getDescription() {
        Map<Integer, Optional<String>> descriptionMap = new LinkedHashMap<>();
        descriptionMap.put(R.string.game_object_info_owner, Optional.of(getOwnerName()));
        descriptionMap.put(R.string.structure_info_left, Optional.of(this.left + " / " + this.total));
        return descriptionMap;
    }

    /** {@inheritDoc} **/
    @Override
    public int getLeftQuantity() {
        return this.left;
    }

    @Override
    public void setLeft(int value) {
        this.left = value;
    }

}

package com.example.poleiskrieg.logics.game.model.objects.structures;

import com.example.poleiskrieg.logics.game.model.resources.Resource;

/**
 * Models every Structure that can produce a specific Resource.
 */
public interface ResourceProducer extends OwnableStructure {

    /**
     * @return the resource produced
     */
    Resource getResource();

    /**
     * @return the quantity produced
     */
    int getProducedQuantity();

}

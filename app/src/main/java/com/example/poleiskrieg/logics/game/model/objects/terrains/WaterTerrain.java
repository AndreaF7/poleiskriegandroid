package com.example.poleiskrieg.logics.game.model.objects.terrains;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.abilities.Ability;
import com.example.poleiskrieg.logics.game.model.abilities.BasicAbilities;
import com.example.poleiskrieg.logics.game.model.objects.AbstractGameObject;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * The water structure on which only ships can go and only ports can be built
 * on.
 */
public class WaterTerrain extends AbstractGameObject implements Terrain {

    private static final String ID = "Water";

    /** {@inheritDoc} **/
    @Override
    public Set<Ability> getRequiredAbilities() {
        final Set<Ability> required = new HashSet<>();
        required.add(BasicAbilities.WALKONWATER);
        return Collections.unmodifiableSet(required);
    }

    /** {@inheritDoc} **/
    @Override
    public String getId() {
        return ID;
    }

    /** {@inheritDoc} **/
    @Override
    public Map<Integer, Optional<String>> getDescription() {
        Map<Integer,Optional<String>> descriptionMap = new LinkedHashMap<>();
        descriptionMap.put(R.string.terrain_water_description, Optional.empty());
        return descriptionMap;
    }

    /**{@inheritDoc}**/@Override
    public int getNameId() {
        return R.string.terrain_water;
    }

}

package com.example.poleiskrieg.logics.game.controller.selection;

import android.content.Context;

import com.example.poleiskrieg.logics.game.model.SkillTreeCommands;
import com.example.poleiskrieg.logics.game.model.TurnManagementCommands;
import com.example.poleiskrieg.logics.game.model.map.ObservableGameMap;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.resources.Resource;

import java.util.Map;

/**
 * Enables to play the game using selection commands, masking other "unsafe"
 * commands. It does so combining selection commands and basic commands.
 */
public interface GameCommandsUsingSelection extends SelectionCommands, ObservableGameMap, SkillTreeCommands, TurnManagementCommands {

    /**
     * AMERI. This method cretes the unit and set it to the map.
     * 
     * @param unit to create
     * 
     * @throws IllegalArgumentException if canCreateUnitFromCity returns false
     */
    void createUnitFromSelectedCity(Unit unit);

    /**
     * AMERI.
     * 
     * This method verify if there isn't CONTINUA
     * 
     * @param unit to create
     * 
     * 
     * @return true if a method that creates a unit can be called
     */
    boolean canCreateUnit(Unit unit);

    /**
     * This method could be used to verify if an unit hero is already in game
     *
     * @param unit         is the unit to verify.
     *
     * @return true if he is not in the game.
     */
    boolean isHeroNotInGame(Unit unit);

    /**
     * AMERI.
     * 
     * @return true if the actual selection is a city or a capital and if the owner
     *         of that structure is the actual player.
     */
    boolean canSelectedCityCreate();

    /**
     *
     * @return current player resources map description.
     */
    Map<Resource, String> getCurrentPlayerResourcesDescriptionMap();

    /**
     *
     * @param structureClass specific structure class.
     * @return count of the Stucture of the specific class, that he own.
     */
    int getActualPlayerSpecificStructureCount(Class<? extends Structure> structureClass);

}

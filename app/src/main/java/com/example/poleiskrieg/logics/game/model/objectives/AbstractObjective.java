package com.example.poleiskrieg.logics.game.model.objectives;

import com.example.poleiskrieg.logics.game.model.map.ObservableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;

/**
 * Models an objective.
 */
public abstract class AbstractObjective implements Objective {

    private final int descriptionId;

    /**
     * @param desc the description id of this objective
     */
    AbstractObjective(final int desc) {
        this.descriptionId = desc;
    }

    /** {@inheritDoc} **/
    @Override
    public int getDescriptionId() {
        return this.descriptionId;
    }

    /** {@inheritDoc} **/
    @Override
    public abstract boolean isCompleted(ObservableGameMap actualGameMap, Player player);

}

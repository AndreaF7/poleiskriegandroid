package com.example.poleiskrieg.logics.game.model.resources;

import java.util.Optional;

/**
 * The Resources that consists of a name and could have a modifier.
 */
public enum BasicResources implements Resource {

    /**
     * The Resources.
     */
    GOLD("Gold", Optional.of(500)), WOOD("Wood", Optional.of(500)), POPULATION("Population", Optional.empty());

    private final String nameId;
    private final Optional<Integer> modifier;

    BasicResources(final String nameId, final Optional<Integer> modifier) {
        this.nameId = nameId;
        this.modifier = modifier;
    }

    @Override
    public String getNameId() {
        return this.nameId;
    }

    /** {@inheritDoc} **/
    @Override
    public Optional<Integer> getModifier() {
        return this.modifier;
    }

}

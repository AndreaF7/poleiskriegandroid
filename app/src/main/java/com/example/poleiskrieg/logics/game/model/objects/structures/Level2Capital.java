package com.example.poleiskrieg.logics.game.model.objects.structures;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.logics.game.model.BasicCostImpl;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Optional;

/**
 * The Level2Capital class extends CapitalImpl and represent the second level of a Capital. This level has an unlock cost.
 */
public class Level2Capital extends CapitalImpl {

    private static final double ENEMY_ATTACK_REDUCTION = 0.60;
    private static final int POPULATION_BOOST = 10;
    private static final int NAME_ID = R.string.structure_capital_level2;
    private static final int GOLD_COST = 400;
    private static final int WOOD_COST = 400;

    /**
     * Level2Capital constructor.
     * 
     * @param player that own the capital.
     */
    public Level2Capital(final Optional<Player> player) {
        super(player, NAME_ID, ENEMY_ATTACK_REDUCTION, POPULATION_BOOST,
                new BasicCostImpl(Optional.of(GOLD_COST), Optional.of(WOOD_COST), Optional.empty()));
    }

    public Level2Capital(){this(Optional.empty());}
}

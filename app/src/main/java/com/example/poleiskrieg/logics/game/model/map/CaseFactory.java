package com.example.poleiskrieg.logics.game.model.map;

import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;

/**
 * package protected.
 *
 */
interface CaseFactory {
    Case getEmptyCase(Terrain caseTerrain);
}

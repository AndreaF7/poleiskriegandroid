package com.example.poleiskrieg.network;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.activities.MainActivity;


import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class NetworkCallbacksFactory {
    private final Context context;

    public NetworkCallbacksFactory(Context context){
        this.context = context;
    }

    public ConnectivityManager.NetworkCallback getShowDialogOnConnectionLost(){
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.network_error_title)
                .setMessage(R.string.network_error_message)
                .setPositiveButton(R.string.network_error_positive, (dialog, which) -> {
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                })
                .setNeutralButton(R.string.settings, (dialog, which) -> {
                    setSettingsIntent();
                })
                .setCancelable(false)
                .create();
        return new ConnectivityManager.NetworkCallback(){

            @Override
            public void onAvailable(@NonNull Network network) {
                super.onAvailable(network);
                new Handler(context.getMainLooper()).post(alertDialog::dismiss);
            }

            @Override
            public void onLost(@NonNull Network network) {
                super.onLost(network);
                new Handler(context.getMainLooper()).post(alertDialog::show);
            }

            @Override
            public void onUnavailable() {
                super.onUnavailable();
                new Handler(context.getMainLooper()).post(alertDialog::show);
            }
        };
    }

    private void setSettingsIntent() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_WIRELESS_SETTINGS);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }
}

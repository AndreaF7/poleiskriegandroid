package com.example.poleiskrieg.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NetworkManager {

    private static volatile NetworkManager INSTANCE;

    private final RequestQueue requestQueue;
    private ConnectivityManager connectivityManager;


    private NetworkManager(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public static synchronized NetworkManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new NetworkManager(context);
        }
        return INSTANCE;
    }

    //----------------CONNECTIVITY MANAGER----------------------------

    public ConnectivityManager getConnectivityManager(){
        return connectivityManager;
    }

    public void registerNetworkCallBack(ConnectivityManager.NetworkCallback callback) {

        if (connectivityManager != null) {
            connectivityManager.registerDefaultNetworkCallback(callback);
        }
    }

    public void unregisterCallback(ConnectivityManager.NetworkCallback callback){
        if (connectivityManager != null) {
            connectivityManager.unregisterNetworkCallback(callback);
        }
    }

    public boolean isNetworkConnectionAvailable(){
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    //----------------REQUEST QUEUE----------------------------

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public void cancelRequests(String tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    public void createRequest(String url, String tag) {
        // Instantiate the RequestQueue.
        // Request a jsonArray response from the provided URL.
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject jsonobject = response.getJSONObject(0);
                            //setTextView(jsonobject.get("lat").toString(), jsonobject.get("lon").toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //setTextView("/", "/");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        jsonArrayRequest.setTag(tag);
        // Add the request to the RequestQueue.
        requestQueue.add(jsonArrayRequest);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}

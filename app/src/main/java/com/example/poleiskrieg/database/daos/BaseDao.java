package com.example.poleiskrieg.database.daos;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Entity;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import java.util.List;

abstract class BaseDao<T> {


    /**
     * Insert an array of objects in the database.
     *
     * @param elements the objects to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract List<Long> insert(List<T> elements);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Long insert(T element);

    /**
     * Update an object from the database.
     *
     * @param elements the object to be updated
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract int update(List<T> elements);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract int update(T element);

    /**
     * Delete an object from the database
     *
     * @param elements the object to be deleted
     */
    @Delete
    public abstract int delete(List<T> elements);

    @Delete
    public abstract int delete(T element);
}

package com.example.poleiskrieg.database.converters;

import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.logics.game.model.objects.GameObject;
import com.example.poleiskrieg.logics.game.model.objects.structures.OwnableStructure;
import com.example.poleiskrieg.logics.game.model.objects.structures.ResourceProducer;
import com.example.poleiskrieg.logics.game.model.objects.structures.ResourceProducerScalable;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Vehicle;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.player.PlayerImpl;
import com.example.poleiskrieg.logics.game.model.resources.Resource;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

public class ClassEntityConverters {
    public static StructureEntity structureToEntity(Structure structure, int x, int y, long gameId) {
        StructureEntity structureEntity = new StructureEntity();
        structureEntity.x = x;
        structureEntity.y = y;
        structureEntity.gameId = gameId;

        structureEntity.className = structure.getClass().getName();
        structureEntity.produced = structure instanceof ResourceProducer ? ((ResourceProducer) structure).getProducedQuantity() : -1;
        structureEntity.remaining = structure instanceof ResourceProducerScalable ? ((ResourceProducerScalable) structure).getLeftQuantity() : -1;
        structureEntity.ownerPlayerId = structure.getOwner().isPresent() ? structure.getOwner().get().getId() : -1;
        structureEntity.conquerorPlayerId = structure instanceof OwnableStructure ? (((OwnableStructure) structure).getConqueror().isPresent() ? ((OwnableStructure) structure).getConqueror().get().getId() : -1) : -1;

        return structureEntity;
    }

    //Once parsed set the owner and the passenger(if vehicle)
    public static UnitEntity unitToEntity(Unit unit, int x, int y, long gameId) {
        UnitEntity unitEntity = new UnitEntity();
        unitEntity.x = x;
        unitEntity.y = y;
        unitEntity.gameId = gameId;
        unitEntity.isVehicle = unit instanceof Vehicle;

        unitEntity.ownerPlayerId = unit.getOwner().isPresent() ? unit.getOwner().get().getId() : -1;
        unitEntity.className = unit.getClass().getName();
        unitEntity.hp = unit.getHp();
        unitEntity.attackCount = unit.getAttackCount();
        unitEntity.didFirstMovement = unit.didFirstMovement();
        unitEntity.movedAfterAttack = unit.movedAfterAttack();

        return unitEntity;
    }

    public static Unit entityToUnit(UnitEntity unitEntity, Optional<Player> owner) {
        Unit unit = null;
        try {
            Class<?> clazz = Class.forName(unitEntity.className);
            if (owner.isPresent()) {
                Constructor<?> ctor = clazz.getConstructor(Player.class);
                unit = (Unit) ctor.newInstance(owner.get());
            } else {
                Constructor<?> ctor = clazz.getConstructor();
                unit = (Unit) ctor.newInstance();
            }
            unit.setMoveAfterAttack(unitEntity.movedAfterAttack);
            unit.setAttackCount(unitEntity.attackCount);
            unit.setHp(unitEntity.hp);
            unit.setDidFirstMove(unitEntity.didFirstMovement);
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalArgumentException();
        }
        return unit;
    }

    public static Vehicle entityToVehicle(UnitEntity vehicleEntity, Optional<Unit> passenger, Optional<Player> owner) {
        Vehicle vehicle = null;
        try {
            Class<?> clazz = Class.forName(vehicleEntity.className);
            Constructor<?> ctor = clazz.getConstructor(Optional.class);
            vehicle = (Vehicle) ctor.newInstance(passenger);
            vehicle.setMoveAfterAttack(vehicleEntity.movedAfterAttack);
            vehicle.setAttackCount(vehicleEntity.attackCount);
            vehicle.setHp(vehicleEntity.hp);
            vehicle.setDidFirstMove(vehicleEntity.didFirstMovement);
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalArgumentException();
        }
        return vehicle;
    }

    public static CaseEntity terrainToCaseEntity(Terrain terrain, int x, int y, long gameId) {
        CaseEntity caseEntity = new CaseEntity();
        caseEntity.x = x;
        caseEntity.y = y;
        caseEntity.gameId = gameId;

        caseEntity.terrain = terrain;
        return caseEntity;
    }

    public static PlayerEntity playerToEntity(Player player, long gameId) {
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.gameId = gameId;
        playerEntity.id = player.getId();
        playerEntity.name = player.getName();
        playerEntity.objective = player.getObjective();
        playerEntity.race = player.getRace();

        return playerEntity;
    }

    public static Player entityToPLayer(PlayerEntity playerEntity) {
        return new PlayerImpl(playerEntity.name, playerEntity.id, playerEntity.race, playerEntity.objective);
    }

    public static SkillTreeAttributeEntity skillTreeAttributeToEntity(long gameId, Player player, SkillTreeAttribute skillTreeAttribute) {
        SkillTreeAttributeEntity skillTreeAttributeEntity = new SkillTreeAttributeEntity();
        skillTreeAttributeEntity.gameId = gameId;
        skillTreeAttributeEntity.playerOwnerId = player.getId();
        skillTreeAttributeEntity.className = skillTreeAttribute.getClass().getName();
        skillTreeAttributeEntity.currentLevel = skillTreeAttribute.getCurrentValue();
        return skillTreeAttributeEntity;
    }

    public static SkillTreeAttribute entityToSkillTreeAttribute(SkillTreeAttributeEntity skillTreeAttributeEntity) {
        SkillTreeAttribute result = null;
        try {
            result = (SkillTreeAttribute) Class.forName(skillTreeAttributeEntity.className).newInstance();
            while (result.getCurrentValue() < skillTreeAttributeEntity.currentLevel) {
                result.upgrade();
            }
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResourceEntity resourceToResourceEntity(long gameId, Player player, Resource resource, int actual, int max) {
        ResourceEntity resourceEntity = new ResourceEntity();
        resourceEntity.gameId = gameId;
        resourceEntity.playerOwnerId = player.getId();
        resourceEntity.resource = resource;
        resourceEntity.actualQuantity = actual;
        resourceEntity.maxQuantity = max;
        return resourceEntity;
    }

}

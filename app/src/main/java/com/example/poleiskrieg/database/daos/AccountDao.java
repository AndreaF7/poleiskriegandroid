package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.AccountEntity;

import java.util.Optional;

@Dao
public abstract class AccountDao extends BaseDao<AccountEntity> {
    @Transaction
    @Query("SELECT * FROM AccountEntity")
    public abstract AccountEntity getLoggedPlayer();
}

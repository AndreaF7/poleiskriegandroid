package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;

import java.util.List;

@Dao
public abstract class StructureDao extends BaseDao<StructureEntity> {
    @Transaction
    @Query("SELECT * FROM StructureEntity WHERE gameId = :gameId AND x = :x AND y = :y")
    public abstract StructureEntity getById(long gameId, int x, int y);

    @Transaction
    @Query("SELECT * FROM StructureEntity WHERE gameId = :gameId")
    public abstract List<StructureEntity> getByGameId(long gameId);

    @Transaction
    @Query("DELETE FROM StructureEntity WHERE gameId = :gameId")
    public abstract void deleteByGameId(long gameId);
}

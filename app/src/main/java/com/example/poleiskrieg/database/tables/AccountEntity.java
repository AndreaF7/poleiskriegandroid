package com.example.poleiskrieg.database.tables;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class AccountEntity {
    @PrimaryKey
    @NonNull
    public String username;

    @ColumnInfo
    public String password;
}

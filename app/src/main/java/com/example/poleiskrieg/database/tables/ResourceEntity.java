package com.example.poleiskrieg.database.tables;

import androidx.annotation.NonNull;
import androidx.room.Entity;

import com.example.poleiskrieg.logics.game.model.resources.Resource;

@Entity(primaryKeys = {"playerOwnerId", "gameId", "resource"})
public class ResourceEntity {
    public int playerOwnerId;
    public long gameId;
    public @NonNull Resource resource;

    public int actualQuantity;
    public int maxQuantity;
}


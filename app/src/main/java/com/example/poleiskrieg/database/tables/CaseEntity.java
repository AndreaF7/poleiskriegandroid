package com.example.poleiskrieg.database.tables;

import androidx.room.Entity;

import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;

import java.util.Optional;

@Entity(primaryKeys = {"gameId", "x", "y"})
public class CaseEntity {
    public long gameId;
    public int x;
    public int y;

    public Terrain terrain;
}

package com.example.poleiskrieg.database.tables;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.model.races.Race;
import com.example.poleiskrieg.util.C;

@Entity(primaryKeys = {"id", "gameId"})
public class PlayerEntity {
    public long gameId;
    public int id;

    @ColumnInfo(defaultValue = C.NOT_LOGGED_IN)
    public String playerAccountId; //-1 if offline
    public String name;
    public Race race;
    public Objective objective;
    @ColumnInfo(defaultValue = "false")
    public boolean eliminated;

    public PlayerEntity(){
        playerAccountId = C.NOT_LOGGED_IN;
        eliminated = false;
    }

}

package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;

import java.util.List;

@Dao
public abstract class CaseDao extends BaseDao<CaseEntity> {
    @Transaction
    @Query("SELECT * FROM CaseEntity WHERE gameId = :gameId AND x = :x AND y = :y")
    public abstract CaseEntity getById(long gameId, int x, int y);

    @Transaction
    @Query("SELECT * FROM CaseEntity WHERE gameId = :gameId")
    public abstract List<CaseEntity> getByGameId(long gameId);

    @Transaction
    @Query("DELETE FROM CaseEntity WHERE gameId = :gameId")
    public abstract void deleteByGameId(long gameId);
}

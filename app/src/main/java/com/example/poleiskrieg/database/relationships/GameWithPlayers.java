package com.example.poleiskrieg.database.relationships;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;

import java.util.List;

public class GameWithPlayers {
    @Embedded public GameEntity gameEntity;
    @Relation(
            parentColumn = "id",
            entityColumn = "gameId"
    )
    public List<PlayerEntity> playerEntityList;
}

package com.example.poleiskrieg.database.converters;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.objectives.Objective;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.races.Race;
import com.example.poleiskrieg.logics.game.model.resources.Resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.example.poleiskrieg.database.converters.TypeConverters.*;

public class EntityJsonConverters {

    private EntityJsonConverters() {

    }

    public static <T> T jsonObjectToEntity(Class<T> entityClass, JSONObject jsonObject) {
        try {
            T entity = entityClass.newInstance();
            for (Field field : entityClass.getFields()) {
                Object value = jsonObject.get(field.getName());
                if (Date.class.isAssignableFrom(field.getType())) {
                    value = fromTimestamp((Long) value);
                } else if (Race.class.isAssignableFrom(field.getType())) {
                    value = nameToRace((String) value);
                } else if (GameRules.class.isAssignableFrom(field.getType())) {
                    value = nameToGameRules((String) value);
                } else if (Objective.class.isAssignableFrom(field.getType())) {
                    value = nameToObjective((String) value);
                } else if (Resource.class.isAssignableFrom(field.getType())) {
                    value = nameToResource((String) value);
                } else if (Terrain.class.isAssignableFrom(field.getType())) {
                    value = nameToTerrain((String) value);
                } else if (boolean.class.isAssignableFrom(field.getType()) && Integer.class.isAssignableFrom(value.getClass())) {
                    value = ((Integer) value) == 1;
                }
                field.set(entity, value);
            }
            return entity;
        } catch (IllegalAccessException | InstantiationException | JSONException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static <T> JSONObject entityToJsonObject(T entity) {
        JSONObject result = new JSONObject();
        for (Field field : entity.getClass().getFields()) {
            try {
                Object value = field.get(entity);
                if (value instanceof Date) {
                    value = dateToTimestamp((Date) value);
                } else if (value instanceof Race) {
                    value = raceToName((Race) value);
                } else if (value instanceof GameRules) {
                    value = gameRulesToName((GameRules) value);
                } else if (value instanceof Objective) {
                    value = objectiveRulesToName((Objective) value);
                } else if (value instanceof Resource) {
                    value = resourceToName((Resource) value);
                } else if (value instanceof Terrain) {
                    value = terrainToName((Terrain) value);
                }
                result.put(field.getName(), value);
            } catch (IllegalAccessException | JSONException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return result;
    }

    public static <T> JSONArray entitiesToJsonArray(List<T> entities) {
        JSONArray result = new JSONArray();
        entities.forEach(playerEntity -> result.put(entityToJsonObject(playerEntity)));
        return result;
    }

    public static <T> List<T> jsonArrayToEntities(Class<T> entityClass, JSONArray jsons) {
        List<T> result = new ArrayList<>();
        for (int i = 0; i < jsons.length(); i++) {
            try {
                result.add(jsonObjectToEntity(entityClass, jsons.getJSONObject(i)));
            } catch (JSONException e) {
                throw new IllegalArgumentException(e);
            }
        }
        return result;
    }
}

package com.example.poleiskrieg.database.pojos;

import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.managers.BasicResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.managers.TurnManager;
import com.example.poleiskrieg.logics.game.model.map.ModifiableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;

import java.util.Date;
import java.util.List;

public class GameInfo {
    public GameRules rules;
    public TurnManager turnManager;
    public BasicResourceManager resources;
    public SkillTreeManager skillTreesManager;
    public ModifiableGameMap map;
    public boolean firstTurn = true;
    public long gameId;
    public long onlineId;
    public List<Player> players;
    public String gameName;
    public Date dateSaved;
}

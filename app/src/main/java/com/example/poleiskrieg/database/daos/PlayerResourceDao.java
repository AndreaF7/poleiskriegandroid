package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.logics.game.model.resources.Resource;

import java.util.List;

@Dao
public abstract class PlayerResourceDao extends BaseDao<ResourceEntity> {
    @Transaction
    @Query("SELECT * FROM ResourceEntity WHERE gameId = :gameId AND playerOwnerId = :playerOwnerId AND resource = :resource")
    public abstract ResourceEntity getById(long gameId, int playerOwnerId, Resource resource);

    @Transaction
    @Query("SELECT * FROM ResourceEntity WHERE gameId = :gameId AND playerOwnerId = :playerOwnerId")
    public abstract List<ResourceEntity> getByPlayer(long gameId, int playerOwnerId);

    @Transaction
    @Query("SELECT * FROM ResourceEntity WHERE gameId = :gameId")
    public abstract List<ResourceEntity> getByGameId(long gameId);

    @Transaction
    @Query("DELETE FROM ResourceEntity WHERE gameId = :gameId")
    public abstract void deleteByGameId(long gameId);
}

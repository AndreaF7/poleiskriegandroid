package com.example.poleiskrieg.database.daos;

import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.GameEntity;

import java.util.Date;
import java.util.List;

@Dao
public abstract class GameDao extends BaseDao<GameEntity> {

    @Query("UPDATE GameEntity SET turnPlayerId = :turnPlayerId, firstTurn = :firstTurn, isValid = :isValid, dateSaved = :dateSaved WHERE id = :id")
    public abstract int update(long id, int turnPlayerId, boolean firstTurn, boolean isValid, Date dateSaved);

    @Transaction
    @Query("SELECT * FROM GameEntity")
    public abstract List<GameEntity> getAll();

    @Transaction
    @Query("SELECT * FROM GameEntity WHERE id = :id")
    public abstract GameEntity getById(long id);

    @Transaction
    @Query("SELECT * FROM GameEntity WHERE onlineId = :onlineId")
    public abstract GameEntity getByOnlineId(long onlineId);

    @Transaction
    @Query("SELECT * FROM GameEntity WHERE onlineId <> -1")
    public abstract List<GameEntity> getOnlineGames();

    @Transaction
    @Query("SELECT * FROM GameEntity")
    public abstract List<GameWithPlayers> getGamesWithPlayers();

    @Transaction
    @Query("SELECT * FROM GameEntity WHERE id = :id")
    public abstract GameWithPlayers getGamesWithPlayersById(long id);

    @Transaction
    @Query("DELETE FROM GameEntity WHERE id = :gameId")
    public abstract void deleteById(long gameId);

    @Transaction
    @Query("DELETE FROM GameEntity WHERE onlineId <> -1")
    public abstract void deleteOnlineGames();

}

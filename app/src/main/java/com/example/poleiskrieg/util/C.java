package com.example.poleiskrieg.util;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.example.poleiskrieg.R;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class C {
    public static final int RC_SIGN_IN = 10;
    public static final String DEBUG_TAG = "POLEIS_KRIEG";
    public static final int LEFT = 0;
    public static final int TOP = 1;
    public static final int RIGHT = 2;
    public static final int BOTTOM = 3;
    public static final String NEUTRAL = "Neutral" ;
    public static final String NUMBER_OF_PLAYERS = "NUMBER_OF_PLAYERS";
    public static final String GAME_MODE = "GAME_MODE";
    public static final String GAME_ACTUAL_PLAYER_ID = "GAME_ACTUAL_PLAYER_ID";
    public static final String GAME_ID = "GAME_ID";
    public static final String PLAYER_ID = "PLAYER_ID";
    public static final String ONLINE_ID = "ONLINE_ID";
    public static final String NOT_LOGGED_IN = "-1";

    public static final String ROOT_ADDRESS = "http://165.22.16.159/";
    public static final String SERVER_ADDRESS = ROOT_ADDRESS + "poleis_krieg_server/";
    public static final String SAVE_GAME_ADDRESS = SERVER_ADDRESS + "save_game.php";
    public static final String CREATE_GAME_ADDRESS = SERVER_ADDRESS + "create_game.php";
    public static final String ADD_PLAYER_ADDRESS = SERVER_ADDRESS + "add_player.php";
    public static final String GET_GAME_INFO_ADDRESS = SERVER_ADDRESS + "get_game_info.php";
    public static final String GET_GAME_BY_ID_ADDRESS = SERVER_ADDRESS + "get_game_by_id.php";
//    public static final String GET_GAME_INFO_BY_ID_ADDRESS = SERVER_ADDRESS + "get_game_info_by_id.php";
    public static final String DELETE_GAME_ADDRESS = SERVER_ADDRESS + "delete_game.php";
    public static final String ADD_ACCOUNT_ADDRESS = SERVER_ADDRESS + "add_account.php";
    public static final String LOG_IN_ADDRESS = SERVER_ADDRESS + "login.php";

    public static final int LOCAL_USER_ID = 1;

    private static Optional<String> loggedPlayerUsername = Optional.empty();

    public static int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static SpannableStringBuilder descriptionToString(Map<Integer, Optional<String>> description, Context context, boolean isInstanced){
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        description.entrySet().forEach(e -> {
            if(isInstanced || !e.getKey().equals(R.string.unit_info_remaining_attacks) && !e.getKey().equals(R.string.unit_info_can_move) && !e.getKey().equals(R.string.game_object_info_owner)){
                if(!e.getKey().equals(R.string.vehicle_info_passenger_info)){
                    stringBuilder.append("- ");
                } else {
                    stringBuilder.append("\n");
                }
                stringBuilder.append(context.getResources().getString(e.getKey())).append(" ");
                stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), stringBuilder.length() - context.getResources().getString(e.getKey()).length(), stringBuilder.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                if(e.getValue().isPresent()){
                    if(e.getValue().get().equals(C.NEUTRAL)){
                        stringBuilder.append(context.getResources().getString(R.string.game_object_owner_neutral));
                    } else if (e.getValue().get().equals("true")){
                        stringBuilder.append(context.getResources().getString(R.string.true_text));
                    } else if (e.getValue().get().equals("false")){
                        stringBuilder.append(context.getResources().getString(R.string.false_text));
                    } else {
                        stringBuilder.append(e.getValue().get());
                    }
                }
                stringBuilder.append("\n");
            }
        });
        return stringBuilder;
    }

    public static void setLoggedPLayer(Optional<String> player){
        loggedPlayerUsername = player;
    }

    public static Optional<String> getLoggedPlayerUsername(){
        return loggedPlayerUsername;
    }

    public static SpannableStringBuilder savedataToString(Map<Object, Optional<Object>> description, Context context){
        AtomicInteger index = new AtomicInteger();
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        description.entrySet().forEach(e -> {
            if(e.getKey() instanceof Integer){
                stringBuilder.append("- ").append(context.getResources().getString((Integer) e.getKey())).append(" ");
                stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), stringBuilder.length() - context.getResources().getString((Integer) e.getKey()).length(), stringBuilder.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (e.getKey() instanceof String) {
                stringBuilder.append("\t").append(String.valueOf(index.incrementAndGet())).append(") ").append(String.valueOf(e.getKey())).append(" ");
                stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), stringBuilder.length() - ((String) e.getKey()).length(), stringBuilder.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if(e.getValue().isPresent()){
                if(e.getValue().get() instanceof Integer){
                    stringBuilder.append(context.getResources().getString((Integer) e.getValue().get()));
                } else if (e.getValue().get() instanceof String) {
                    stringBuilder.append((String) e.getValue().get());
                }
            }
            stringBuilder.append("\n");
        });
        return stringBuilder;
    }

    public static SpannableStringBuilder playerInfoToString(Map<Object, Optional<Object>> description, Context context){
        AtomicInteger index = new AtomicInteger();
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        description.entrySet().forEach(e -> {
            stringBuilder.append("- ");
            if(e.getKey() instanceof Integer){
                stringBuilder.append(context.getResources().getString((Integer) e.getKey())).append(" ");
                stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), stringBuilder.length() - context.getResources().getString((Integer) e.getKey()).length(), stringBuilder.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (e.getKey() instanceof String) {
                stringBuilder.append(String.valueOf(index.incrementAndGet())).append(") ").append(String.valueOf(e.getKey())).append(" ");
                stringBuilder.setSpan(new StyleSpan(Typeface.BOLD), stringBuilder.length() - ((String) e.getKey()).length(), stringBuilder.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            if(e.getValue().isPresent()){
                if(e.getValue().get() instanceof Integer){
                    stringBuilder.append(context.getResources().getString((Integer) e.getValue().get()));
                } else if (e.getValue().get() instanceof String) {
                    stringBuilder.append((String) e.getValue().get());
                }
            }
            stringBuilder.append("\n");
        });
        return stringBuilder;
    }

    public static Map<String, Integer> getSupportedLanguages(){
        Map<String, Integer> languages = new LinkedHashMap<>();
        languages.put("it-IT", R.string.language_it);
        languages.put("en-US", R.string.language_en);
        return languages;
    }

    public static List<Integer> getOrientationScreenModes(){
        List<Integer> orientationScreenModes = new LinkedList<>();
        orientationScreenModes.add(R.string.screen_auto);
        orientationScreenModes.add(R.string.screen_landscape);
        orientationScreenModes.add(R.string.screen_portrait);
        return orientationScreenModes;
    }

    public static void doClickFeeling(View view){
        Animation animation = new AlphaAnimation(1, 0.5f);  //to change visibility from visible to half-visible
        animation.setDuration(50);                          // 100 millisecond duration for each animation cycle
        animation.setRepeatMode(Animation.REVERSE);         //animation will start from end point once ended.
        view.startAnimation(animation);                     //to start animation
    }
}

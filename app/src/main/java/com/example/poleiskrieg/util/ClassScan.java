package com.example.poleiskrieg.util;

import android.content.Context;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ClassScan {


    private static final ClassScan singleton = new ClassScan();

    private static final List<String> SCAN_KEYS = new ArrayList<>(Arrays.asList("SKILL_TREE_ATTRIBUTE", "TERRAIN", "GAME_RULE", "GAME_OBJECT", "RACE", "STRUCTURE"));

    private final Map<String, List<String>> scanResults;

    public static final ClassScan get(){
        return singleton;
    }

    private ClassScan() {
        this.scanResults = new LinkedHashMap<>();
    }

    public List<String> getSkilltreeAttributeClasses() {
        return scanResults.get(SCAN_KEYS.get(0));
    }

    public List<String> getTerrainClasses() {
        return scanResults.get(SCAN_KEYS.get(1));
    }

    public List<String> getGameRuleClasses() {
        return scanResults.get(SCAN_KEYS.get(2));
    }

    public List<String> getGameObjectClasses() {
        return scanResults.get(SCAN_KEYS.get(3));
    }

    public List<String> getRaceClasses() {
        return scanResults.get(SCAN_KEYS.get(4));
    }

    public List<String> getStructureClasses() {
        return scanResults.get(SCAN_KEYS.get(5));
    }


    public static void setConfigurationFile(int resourceId, Context context) {
        JSONParser jsonParser = new JSONParser();
        Object obj = new JSONObject();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(resourceId))))
        {
            //Read JSON file
            obj = jsonParser.parse(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final JSONObject resultList = (JSONObject) obj;
        SCAN_KEYS.forEach(s -> {
            ClassScan.get().scanResults.put(s, (List<String>) resultList.get(s));
        });

    }


}

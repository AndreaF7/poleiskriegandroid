package com.example.poleiskrieg.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.element.CreateMenuElement;
import com.example.poleiskrieg.element.ImageWithText;
import com.example.poleiskrieg.logics.game.controller.ModelToViewConverterUtils;
import com.example.poleiskrieg.util.C;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class CreateMenuElementAdapter extends RecyclerView.Adapter<CreateMenuElementAdapter.AdapterViewHolder> implements ImageWithTextAdapter.OnButtonClickListener{

    private ArrayList<CreateMenuElement> list;
    private CreateMenuElementAdapter.OnButtonClickListener onButtonClickListener;
    private Context context;

    public CreateMenuElementAdapter(ArrayList<CreateMenuElement> list, CreateMenuElementAdapter.OnButtonClickListener onButtonClickListener, Context context) {
        this.list = list;
        this.onButtonClickListener = onButtonClickListener;
        this.context = context;
    }

    public void setList(ArrayList<CreateMenuElement> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public CreateMenuElementAdapter.AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_create_menu_element, parent, false);
        return new CreateMenuElementAdapter.AdapterViewHolder(view, this.onButtonClickListener);
    }

    @Override
    public void onBindViewHolder(CreateMenuElementAdapter.AdapterViewHolder holder, int position) {
        CreateMenuElement currentItem = list.get(position);
        holder.playerImage.setImageResource(currentItem.getImageResource());
        holder.unitName.setText(currentItem.getNameTextResource());
        holder.actionButton.setText(currentItem.getActionText());
        if(currentItem.getCostResource().getCost().isPresent()){
            ArrayList<ImageWithText<String>> list = new ArrayList<>();
            currentItem.getCostResource().getCost().get().entrySet().forEach(e -> {
                list.add(new ImageWithText<>(C.getResId(ModelToViewConverterUtils.modelResourceToViewId(e.getKey()), R.drawable.class), e.getValue().toString()));
            });
            holder.recyclerViewResources.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false);
            ImageWithTextAdapter<String> adapter = new ImageWithTextAdapter<>(list, this);
            holder.recyclerViewResources.setLayoutManager(layoutManager);
            holder.recyclerViewResources.setAdapter(adapter);
        }
        if(currentItem.getCanCreateResource()){
            holder.warningText.setVisibility(View.INVISIBLE);
            holder.actionButton.setVisibility(View.VISIBLE);
        } else {
            if(currentItem.getIsHeroNotInGameResource()){
                holder.warningText.setText(R.string.cant_create_text);
            } else {
                holder.warningText.setText(R.string.hero_already_in_game);
            }
            holder.actionButton.setVisibility(View.INVISIBLE);
            holder.warningText.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onButtonClick(int position) {

    }


    /** AdapterViewHolder class*/
    public static class AdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ButtonWithFeeling infoButton;
        private ButtonWithFeeling actionButton;
        private ImageView playerImage;
        private TextView unitName;
        private TextView warningText;
        private RecyclerView recyclerViewResources;
        CreateMenuElementAdapter.OnButtonClickListener onButtonClickListener;

        public AdapterViewHolder(View itemView, CreateMenuElementAdapter.OnButtonClickListener onButtonClickListener) {
            super(itemView);
            this.infoButton = itemView.findViewById(R.id.infoButton);
            this.actionButton = itemView.findViewById(R.id.createButton);
            this.playerImage = itemView.findViewById(R.id.objectImage);
            this.unitName = itemView.findViewById(R.id.unitText);
            this.warningText = itemView.findViewById(R.id.warningText);
            this.recyclerViewResources = itemView.findViewById(R.id.recyclerViewResources);
            this.onButtonClickListener = onButtonClickListener;
            infoButton.setOnClickListener(this);
            actionButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            try {
                this.onButtonClickListener.onButtonClick(getAdapterPosition(), view.getId());
            } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /** OnButtonClickListener interface*/

    public interface OnButtonClickListener{
        void onButtonClick(int position, int buttonId) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException;
    }
}
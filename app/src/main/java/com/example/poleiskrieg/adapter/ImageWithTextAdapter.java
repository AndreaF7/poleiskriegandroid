package com.example.poleiskrieg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.poleiskrieg.R;
import com.example.poleiskrieg.element.ButtonTextImage;
import com.example.poleiskrieg.element.ButtonWithFeeling;
import com.example.poleiskrieg.element.ImageWithText;

import java.util.ArrayList;

public class ImageWithTextAdapter<X> extends RecyclerView.Adapter<ImageWithTextAdapter.AdapterViewHolder>{

    private ArrayList<ImageWithText<X>> list;
    private ImageWithTextAdapter.OnButtonClickListener onButtonClickListener;

    public ImageWithTextAdapter(ArrayList<ImageWithText<X>> list, ImageWithTextAdapter.OnButtonClickListener onButtonClickListener) {
        this.list = list;
        this.onButtonClickListener = onButtonClickListener;
    }

    @Override
    public ImageWithTextAdapter.AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_image_text, parent, false);
        return new ImageWithTextAdapter.AdapterViewHolder(view, this.onButtonClickListener);
    }

    @Override
    public void onBindViewHolder(ImageWithTextAdapter.AdapterViewHolder holder, int position) {
        ImageWithText<X> currentItem = list.get(position);
        holder.imageView.setImageResource(currentItem.getImageResource());
        if(currentItem.getTextResource() instanceof Integer){
            holder.textView.setText((Integer)currentItem.getTextResource());
        }
        if(currentItem.getTextResource() instanceof String){
            holder.textView.setText((String)currentItem.getTextResource());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    /** AdapterViewHolder class*/
    public static class AdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageView;
        private TextView textView;
        ImageWithTextAdapter.OnButtonClickListener onButtonClickListener;

        private AdapterViewHolder(View itemView, ImageWithTextAdapter.OnButtonClickListener onButtonClickListener) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            this.onButtonClickListener = onButtonClickListener;
        }

        @Override
        public void onClick(View view) {
        }
    }

    /** OnButtonClickListener interface*/

    public interface OnButtonClickListener{
        void onButtonClick(int position);
    }
}

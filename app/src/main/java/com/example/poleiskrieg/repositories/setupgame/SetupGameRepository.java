package com.example.poleiskrieg.repositories.setupgame;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.util.Pair;
import com.example.poleiskrieg.repositories.DatabaseRepository;
import com.example.poleiskrieg.util.C;

import java.util.Date;
import java.util.concurrent.Callable;

public class SetupGameRepository extends DatabaseRepository {


    public SetupGameRepository(Application application) {
        super(application);
    }

    public long createGame(String gameName, int numberOfPLayers, GameRules gameRules) {
        Callable<Long> f = () -> {
            GameEntity game = getSetupGameEntity(gameName, numberOfPLayers, gameRules);
            long id = db.getGameDao().insert(game);
            Log.d(C.DEBUG_TAG, String.valueOf(id));
            return id;

        };
        return submitOnDatabase(f);
    }

    protected GameEntity getSetupGameEntity(String gameName, int numberOfPLayers, GameRules gameRules){
        Pair<Integer, Integer> mapDimensions = computeMapDimensions(numberOfPLayers);
        GameEntity game = new GameEntity();
        game.numberOfPlayers = numberOfPLayers;
        game.mapHeight = mapDimensions.getKey();
        game.mapWidth = mapDimensions.getValue();
        game.turnPlayerId = 1;
        game.gameRules = gameRules;
        game.firstTurn = true;
        game.isValid = false;
        game.gameName = gameName;
        game.dateSaved = new Date();
        //game.creatorId = submitOnDatabase(() -> db.getPlayerAccountDao().getLoggedInAccount().id); TODO see why it does not work
        game.creatorId = C.getLoggedPlayerUsername().isPresent() ? C.getLoggedPlayerUsername().get() : C.NOT_LOGGED_IN;
        return game;
    }

    private Pair<Integer, Integer> computeMapDimensions(int numberOfPLayers) {
        int height = 20 ;
        int width = 20 ;//TODO implement
        return new Pair<>(height, width);
    }
}

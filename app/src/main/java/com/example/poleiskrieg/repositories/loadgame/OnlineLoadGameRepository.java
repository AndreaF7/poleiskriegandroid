package com.example.poleiskrieg.repositories.loadgame;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.network.NetworkManager;
import com.example.poleiskrieg.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonArrayToEntities;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonObjectToEntity;

public class OnlineLoadGameRepository extends OfflineLoadGameRepository implements LoadGameRepository {

    private NetworkManager networkManager;
    private final Application application;


    public OnlineLoadGameRepository(Application application) {
        super(application);
        this.application = application;
        networkManager = NetworkManager.getInstance(application);
        updateGames();
    }

    private void deleteOutdatedGames() {
        list.setValue(new ArrayList<>());
        submitOnDatabase(() -> db.getGameDao().getOnlineGames()).stream().map(g -> g.id).forEach(gameId -> this.deleteGame(gameId, false));
    }

    private void deleteOutdatedGamesSameThread() {
        list.setValue(new ArrayList<>());
        submitOnDatabase(() -> db.getGameDao().getOnlineGames()).stream().map(g -> g.id).forEach(gameId -> this.deleteGame(gameId, false));
    }

    public LiveData<Long> searchOnlineGame(long onlineId) {
        MutableLiveData<Long> liveGameId = new MutableLiveData<>();

        //TODO aggiungi controllo se game pieno
        JSONArray postParameters = new JSONArray();
        JSONObject postArgs = new JSONObject();
        try {
            postArgs.put("onlineId", onlineId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postParameters.put(postArgs);
        Log.d(C.DEBUG_TAG, String.valueOf(postParameters));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.GET_GAME_BY_ID_ADDRESS,
                postParameters,
                response -> {
                    try {
                        Log.d(C.DEBUG_TAG, "response: " + response);

                        JSONObject updatedGameInfo = response.getJSONObject(0);
                        GameEntity gameEntity = jsonObjectToEntity(GameEntity.class, updatedGameInfo.getJSONObject(GameEntity.class.getSimpleName()));
                        List<PlayerEntity> playerEntities = jsonArrayToEntities(PlayerEntity.class, updatedGameInfo.getJSONArray(PlayerEntity.class.getSimpleName()));
                        if (gameEntity.isValid || playerEntities.size() >= gameEntity.numberOfPlayers) {
                            liveGameId.setValue((long) -3);
                        } else if (playerEntities.stream().anyMatch(p -> p.playerAccountId.equals(C.getLoggedPlayerUsername().get()))) {
                            liveGameId.setValue((long) -4);
                        } else {
                            //assign local ids
                            Optional<GameEntity> savedGame = submitOnDatabase(() -> db.getGameDao().getOnlineGames().stream().filter(ge -> ge.onlineId == onlineId).findFirst());
                            if (savedGame.isPresent()) {
                                gameEntity.id = savedGame.get().id;
                            }
                            long gameId = submitOnDatabase(() -> db.getGameDao().insert(gameEntity));
                            playerEntities.forEach(pE -> {
                                pE.gameId = gameId;
                                executeOnDatabase(() -> db.getPlayerDao().insert(pE));
                            });
                            liveGameId.setValue(gameId);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> {
                    Log.d(C.DEBUG_TAG, error.toString());
                    liveGameId.setValue((long) -2);
                });
        networkManager.addToRequestQueue(request);
        return liveGameId;
    }

    @Override
    public LiveData<List<GameWithPlayers>> getGames() {
        updateGames();
        return list;
    }

    private List<GameEntity> getOnlineGames() {
        return submitOnDatabase(() -> db.getGameDao().getOnlineGames());
    }

    @Override
    public void updateGames() {
        //TODO pull all the info of the game(including the map, if available)
        //deleteOutdatedGames();
        JSONArray postParameters = new JSONArray();
        JSONObject postArgs = new JSONObject();
        try {
            postArgs.put("userId", C.getLoggedPlayerUsername().get());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postParameters.put(postArgs);
        Log.d(C.DEBUG_TAG, String.valueOf(postParameters));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.GET_GAME_INFO_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, "response: " + response);

                    Handler mainHandler = new Handler(application.getMainLooper());
                    mainHandler.post(() -> {
                        executeOnDatabase(() -> {
                            db.getGameDao().getOnlineGames().stream().map(g -> g.id).forEach(gameId -> this.deleteGameFromSameThread(gameId));
                            List<GameWithPlayers> updatedList = new ArrayList<>();
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject updatedGameInfo = response.getJSONObject(i);

                                    GameEntity gameEntity = jsonObjectToEntity(GameEntity.class, updatedGameInfo.getJSONObject(GameEntity.class.getSimpleName()));
                                    long gameId = db.getGameDao().insert(gameEntity);
                                    gameEntity.id = gameId;
                                    List<PlayerEntity> playerEntities = jsonArrayToEntities(PlayerEntity.class, updatedGameInfo.getJSONArray(PlayerEntity.class.getSimpleName()));
                                    playerEntities = playerEntities.stream().peek(p -> p.gameId = gameId).collect(Collectors.toList());
                                    db.getPlayerDao().insert(playerEntities);

                                    GameWithPlayers newValue = new GameWithPlayers();
                                    newValue.gameEntity = gameEntity;
                                    newValue.playerEntityList = playerEntities;
                                    updatedList.add(newValue);

                                    if(gameEntity.isValid){
                                        List<CaseEntity> caseEntities = jsonArrayToEntities(CaseEntity.class, updatedGameInfo.getJSONArray(CaseEntity.class.getSimpleName()));
                                        List<StructureEntity> structureEntities = jsonArrayToEntities(StructureEntity.class, updatedGameInfo.getJSONArray(StructureEntity.class.getSimpleName()));
                                        List<UnitEntity> unitEntities = jsonArrayToEntities(UnitEntity.class, updatedGameInfo.getJSONArray(UnitEntity.class.getSimpleName()));
                                        List<ResourceEntity> resourceEntities = jsonArrayToEntities(ResourceEntity.class, updatedGameInfo.getJSONArray(ResourceEntity.class.getSimpleName()));
                                        List<SkillTreeAttributeEntity> skillTreeAttributeEntities = jsonArrayToEntities(SkillTreeAttributeEntity.class, updatedGameInfo.getJSONArray(SkillTreeAttributeEntity.class.getSimpleName()));
                                        db.getCaseDao().insert(caseEntities.stream().peek(p -> p.gameId = gameId).collect(Collectors.toList()));
                                        db.getStructureDao().insert(structureEntities.stream().peek(p -> p.gameId = gameId).collect(Collectors.toList()));
                                        db.getUnitDao().insert(unitEntities.stream().peek(p -> p.gameId = gameId).collect(Collectors.toList()));
                                        db.getPlayerResourceDao().insert(resourceEntities.stream().peek(p -> p.gameId = gameId).collect(Collectors.toList()));
                                        db.getSkillTreeAttributeDao().insert(skillTreeAttributeEntities.stream().peek(p -> p.gameId = gameId).collect(Collectors.toList()));
                                    }

                                } catch (JSONException e) {
                                    throw new IllegalStateException(e);
                                }
                            }
                            mainHandler.post(() -> {
                                this.list.setValue(updatedList);
                            });
                        });
                    });
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
//        JSONArray postParameters = new JSONArray();
//        JSONObject postArgs = new JSONObject();
//        try {
//            postArgs.put("userId", C.getLoggedPlayerUsername().get());
//        } catch (JSONException e) {
//            throw new IllegalStateException();
//        }
//        postParameters.put(postArgs);
//        Log.d(C.DEBUG_TAG, String.valueOf(postParameters));
//
//        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
//                C.GET_GAME_INFO_ADDRESS,
//                postParameters,
//                response -> {
//                    try {
//                        Log.d(C.DEBUG_TAG, "response: " + response);
//                        List<GameWithPlayers> gamesWithPLayers = new ArrayList<>();
//                        for (int i = 0; i < response.length(); i++) {
//                            GameWithPlayers gameWithPlayers = new GameWithPlayers();
//                            gameWithPlayers.playerEntityList = new ArrayList<>();
//                            JSONObject updatedGameInfo = response.getJSONObject(i);
//                            GameEntity gameEntity = jsonObjectToEntity(GameEntity.class, updatedGameInfo.getJSONObject(GameEntity.class.getSimpleName()));
//                            List<PlayerEntity> playerEntities = jsonArrayToEntities(PlayerEntity.class, updatedGameInfo.getJSONArray(PlayerEntity.class.getSimpleName()));
//                            //assign local ids
//                            long gameId = submitOnDatabase(() -> db.getGameDao().insert(gameEntity));
//                            playerEntities.forEach(pE -> {
//                                pE.gameId = gameId;
//                                executeOnDatabase(() -> db.getPlayerDao().insert(pE));
//                                gameWithPlayers.playerEntityList.add(pE);
//                            });
//                            gameEntity.id = gameId;
//                            gameWithPlayers.gameEntity = gameEntity;
//                            gamesWithPLayers.add(gameWithPlayers);
//                        }
//                        this.list.setValue(gamesWithPLayers);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    Log.d(C.DEBUG_TAG, response.toString());
//                },
//                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
//        networkManager.addToRequestQueue(request);
    }

    @Override
    public void deleteGame(long gameId) {
        long onlineId = submitOnDatabase(() -> db.getGameDao().getById(gameId).onlineId);
        super.deleteGame(gameId);
        JSONObject id = new JSONObject();
        JSONArray postParameters = new JSONArray();
        try {
            id.put("onlineId", onlineId);
            postParameters.put(0, id);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.DELETE_GAME_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
    }
}

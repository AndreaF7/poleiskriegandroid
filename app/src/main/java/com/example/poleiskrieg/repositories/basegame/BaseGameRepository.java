package com.example.poleiskrieg.repositories.basegame;

import android.app.Application;
import android.util.Log;

import com.example.poleiskrieg.database.converters.ClassEntityConverters;
import com.example.poleiskrieg.database.pojos.GameInfo;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.managers.BasicResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.ResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManagerImpl;
import com.example.poleiskrieg.logics.game.model.managers.TurnManager;
import com.example.poleiskrieg.logics.game.model.managers.TurnManagerImpl;
import com.example.poleiskrieg.logics.game.model.map.GameMapFactoryImpl;
import com.example.poleiskrieg.logics.game.model.map.ModifiableGameMap;
import com.example.poleiskrieg.logics.game.model.objects.structures.OwnableStructure;
import com.example.poleiskrieg.logics.game.model.objects.structures.ResourceProducerScalable;
import com.example.poleiskrieg.logics.game.model.objects.structures.Structure;
import com.example.poleiskrieg.logics.game.model.objects.structures.VehicleProducer;
import com.example.poleiskrieg.logics.game.model.objects.terrains.Terrain;
import com.example.poleiskrieg.logics.game.model.objects.unit.Unit;
import com.example.poleiskrieg.logics.game.model.objects.unit.vehicle.Vehicle;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.logics.game.model.resources.Resource;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTree;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeAttribute;
import com.example.poleiskrieg.logics.game.model.skilltree.SkillTreeImpl;
import com.example.poleiskrieg.logics.game.util.Coordinates;
import com.example.poleiskrieg.logics.game.util.Pair;
import com.example.poleiskrieg.repositories.DatabaseRepository;
import com.example.poleiskrieg.util.C;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static com.example.poleiskrieg.database.converters.ClassEntityConverters.entityToUnit;
import static com.example.poleiskrieg.database.converters.ClassEntityConverters.resourceToResourceEntity;
import static com.example.poleiskrieg.database.converters.ClassEntityConverters.skillTreeAttributeToEntity;
import static com.example.poleiskrieg.database.converters.ClassEntityConverters.structureToEntity;
import static com.example.poleiskrieg.database.converters.ClassEntityConverters.terrainToCaseEntity;
import static com.example.poleiskrieg.database.converters.ClassEntityConverters.unitToEntity;
import static com.example.poleiskrieg.database.converters.ClassEntityConverters.entityToVehicle;

public class BaseGameRepository extends DatabaseRepository {

    public BaseGameRepository(Application application) {
        super(application);
    }


    protected long createGameFromSameThread(long gameId, long onlineId) {
        GameWithPlayers gameWithPlayers = db.getGameDao().getGamesWithPlayersById(gameId);
        List<Player> players = gameWithPlayers.playerEntityList.stream().map(ClassEntityConverters::entityToPLayer).collect(Collectors.toList());
        TurnManager turnManager = new TurnManagerImpl(players);
        SkillTreeManagerImpl skillTreeManager = new SkillTreeManagerImpl(players);
        ResourceManager resourceManager = new BasicResourceManager(players, gameWithPlayers.gameEntity.gameRules.getInitialValues());
        ModifiableGameMap map = new GameMapFactoryImpl().gameMapWithIslands(new Pair<Integer, Integer>(gameWithPlayers.gameEntity.mapHeight, gameWithPlayers.gameEntity.mapWidth), new HashSet<Player>(players), skillTreeManager);
        saveGameOnTheSameThread(gameId, turnManager, skillTreeManager, resourceManager, map, gameWithPlayers.gameEntity.gameRules, true, players, gameWithPlayers.gameEntity.gameName);

        return gameId;
    }

    public long createGame(long gameId, long onlineId) {
        Callable<Long> f = () -> createGameFromSameThread(gameId, onlineId);
        return submitOnDatabase(f);
    }

    public long createGame(long gameId) {
        return createGame(gameId, -1);
    }

    public GameInfo getSavedGameData(long gameId) {//TODO don't add eliminated player to turn manager

        return submitOnDatabase(() -> {
            GameEntity game = this.db.getGameDao().getById(gameId);

            List<PlayerEntity> playerEntities = db.getPlayerDao().getByGameId(gameId);
            List<ResourceEntity> resourceEntities = db.getPlayerResourceDao().getByGameId(gameId);
            List<SkillTreeAttributeEntity> skillTreeAttributeEntities = db.getSkillTreeAttributeDao().getByGameId(gameId);
            List<CaseEntity> caseEntities = db.getCaseDao().getByGameId(gameId);
            List<StructureEntity> structureEntities = db.getStructureDao().getByGameId(gameId);
            List<UnitEntity> unitEntities = db.getUnitDao().getByGameId(gameId);

            return getGameInfoFromEntities(game, playerEntities, resourceEntities, skillTreeAttributeEntities, caseEntities, structureEntities, unitEntities);
        });

    }

    public GameInfo getGameInfoFromEntities(GameEntity game, List<PlayerEntity> playerEntities, List<ResourceEntity> resourceEntities, List<SkillTreeAttributeEntity> skillTreeAttributeEntities, List<CaseEntity> caseEntities, List<StructureEntity> structureEntities, List<UnitEntity> unitEntities) {
        GameInfo gameInfo = new GameInfo();
        gameInfo.rules = game.gameRules;
        gameInfo.firstTurn = game.firstTurn;
        gameInfo.dateSaved = game.dateSaved;
        gameInfo.gameId = game.id;
        gameInfo.gameName = game.gameName;
        gameInfo.onlineId = game.onlineId;

        List<Player> players = playerEntities.stream().map(ClassEntityConverters::entityToPLayer).collect(Collectors.toList());
        List<Player> inGamePLayers = playerEntities.stream().filter(p -> !p.eliminated).map(ClassEntityConverters::entityToPLayer).collect(Collectors.toList());
        gameInfo.players = players;
        gameInfo.turnManager = new TurnManagerImpl(inGamePLayers, inGamePLayers.stream().filter(p -> p.getId() == game.turnPlayerId).findFirst().get());


        Map<Player, Map<Resource, Integer>> playersMaxResources = new HashMap<>();
        Map<Player, Map<Resource, Integer>> playersActualResources = new HashMap<>();
        Map<Player, SkillTree> playersSkillTreeAttributes = new HashMap<>();
        players.forEach(player -> {
            //add resources
            List<ResourceEntity> re = resourceEntities.stream().filter(r -> r.playerOwnerId == player.getId()).collect(Collectors.toList());
            Map<Resource, Integer> rMaxMap = new HashMap<>();
            Map<Resource, Integer> rActualMap = new HashMap<>();
            re.forEach(resourceEntity -> {
                rMaxMap.put(resourceEntity.resource, resourceEntity.maxQuantity);
                rActualMap.put(resourceEntity.resource, resourceEntity.actualQuantity);
            });
            playersMaxResources.put(player, rMaxMap);
            playersActualResources.put(player, rActualMap);

            //add skillTreeAttributes
            List<SkillTreeAttributeEntity> skae = skillTreeAttributeEntities.stream().filter(s -> s.playerOwnerId == player.getId()).collect(Collectors.toList());
            SkillTree skillTree = new SkillTreeImpl(skae.stream().map(ClassEntityConverters::entityToSkillTreeAttribute).collect(Collectors.toList()));
            playersSkillTreeAttributes.put(player, skillTree);
        });
        gameInfo.resources = new BasicResourceManager(playersActualResources, playersMaxResources);
        gameInfo.skillTreesManager = new SkillTreeManagerImpl(playersSkillTreeAttributes);

        //reconstruct game map
        Map<Coordinates, Terrain> mapBase = caseEntities.stream().collect(Collectors.toMap(caseEntity -> new Coordinates(caseEntity.x, caseEntity.y), caseEntity -> caseEntity.terrain));
        gameInfo.map = new GameMapFactoryImpl().getEmptyMapFromBackground(mapBase, new Pair<>(game.mapWidth, game.mapHeight));

        Map<Coordinates, StructureEntity> mapStructures = structureEntities.stream().collect(Collectors.toMap(structureEntity -> new Coordinates(structureEntity.x, structureEntity.y), structureEntity -> structureEntity));
        mapStructures.entrySet().forEach(e -> {
            StructureEntity structureEntity = e.getValue();
            Structure structure = null;
            try {
                structure = (Structure) Class.forName(structureEntity.className).newInstance();
            } catch (IllegalAccessException | InstantiationException | ClassNotFoundException ex) {
                Log.d(C.DEBUG_TAG, ex.toString());
            }
            if (structure == null) {
                throw new IllegalArgumentException();
            }
            if (structureEntity.ownerPlayerId > -1) {
                structure.setOwner(players.stream().filter(p -> p.getId() == structureEntity.ownerPlayerId).findFirst().get());
            }
            if (structureEntity.conquerorPlayerId > -1 && structure instanceof OwnableStructure) {
                ((OwnableStructure) structure).initiateConquer(players.stream().filter(p -> p.getId() == structureEntity.conquerorPlayerId).findFirst().get());
            }
            if (structureEntity.produced > -1 && structure instanceof ResourceProducerScalable) {
                ((ResourceProducerScalable) structure).setProduced(structureEntity.produced);
            }
            if (structureEntity.remaining > -1 && structure instanceof ResourceProducerScalable) {
                ((ResourceProducerScalable) structure).setLeft(structureEntity.remaining);
            }
            if (structure instanceof VehicleProducer) {
                ((VehicleProducer) structure).setSkillTreeManager(gameInfo.skillTreesManager);
            }
            try {
                gameInfo.map.setStructure(e.getKey(), structure);
            } catch (Exception ex) {
                throw ex;
            }
        });


        Map<Coordinates, List<UnitEntity>> mapUnits = unitEntities.stream().collect(Collectors.groupingBy(unitEntity -> new Coordinates(unitEntity.x, unitEntity.y)));
        mapUnits.entrySet().forEach(e -> {
            //TODO set owner
            List<UnitEntity> units = e.getValue();
            UnitEntity passenger = units.stream().filter(u -> !u.isVehicle).findFirst().get();
            Unit passengerUnit = null;
            if (passenger.ownerPlayerId > -1) {
                passengerUnit = entityToUnit(passenger, Optional.of(players.stream().filter(p -> p.getId() == passenger.ownerPlayerId).findFirst().get()));
            } else {
                passengerUnit = entityToUnit(passenger, Optional.empty());
            }
            Optional<UnitEntity> vehicleOpt = units.stream().filter(u -> u.isVehicle).findFirst();
            if (vehicleOpt.isPresent()) {
                UnitEntity vehicle = vehicleOpt.get();
                Vehicle vehicleUnit = null;
                if (vehicle.ownerPlayerId > -1) {
//                    vehicleUnit= (Vehicle) entityToUnit(vehicle, Optional.of(players.stream().filter(p -> p.getId() == vehicle.ownerPlayerId).findFirst().get()));
                    vehicleUnit = entityToVehicle(vehicle, Optional.of(passengerUnit), Optional.of(players.stream().filter(p -> p.getId() == vehicle.ownerPlayerId).findFirst().get()));
                } else {
                    passengerUnit = entityToUnit(passenger, Optional.empty());
                }
                vehicleUnit.setPassenger(passengerUnit);
                gameInfo.map.setUnit(e.getKey(), vehicleUnit);
            } else {
                gameInfo.map.setUnit(e.getKey(), passengerUnit);
            }
        });
        return gameInfo;
    }

    protected void saveGameOnTheSameThread(long gameId, TurnManager turnManager, SkillTreeManager skillTreesManager, ResourceManager resources, ModifiableGameMap map, GameRules rules, boolean firstTurn, List<Player> players, String gameName) {
        //save game
        resetGameStateFromSameThread(gameId);
        db.getGameDao().update(gameId, turnManager.getTurnPlayer().getId(), firstTurn, true, new Date());

        //save map
        map.toMap().forEach((cords, elements) -> {
            final CaseEntity caseEntity = new CaseEntity();
            int x = cords.getX();
            int y = cords.getY();
            elements.forEach(e -> {
                if (e instanceof Structure) {
                    db.getStructureDao().insert(structureToEntity((Structure) e, x, y, gameId));
                } else if (e instanceof Unit) {
                    db.getUnitDao().insert(unitToEntity((Unit) e, x, y, gameId));
                    if (e instanceof Vehicle) {
                        Vehicle vehicle = (Vehicle) e;
                        Optional<Unit> passenger = vehicle.getPassenger();
                        if (passenger.isPresent()) {
                            db.getUnitDao().insert(unitToEntity(vehicle.getPassenger().get(), x, y, gameId));
                        }
                    }
                } else if (e instanceof Terrain) {
                    db.getCaseDao().insert(terrainToCaseEntity((Terrain) e, x, y, gameId));
                }
            });
        });

        //save players with their skilltree and resources
        players.forEach(p -> {
            boolean eliminated = !turnManager.getInGamePLayers().contains(p);
            db.getPlayerDao().update(p.getId(), gameId, eliminated);

            List<SkillTreeAttribute> skillTreeAttributes = skillTreesManager.getAllPlayerAttributes(p);
            skillTreeAttributes.forEach(a -> db.getSkillTreeAttributeDao().insert(skillTreeAttributeToEntity(gameId, p, a)));

            Map<Resource, Integer> actualResources = resources.getPlayerResourceMap(p);
            Map<Resource, Integer> maxResources = resources.getPlayerMaxResourceMap(p);

            actualResources.forEach((k, v) -> db.getPlayerResourceDao().insert(resourceToResourceEntity(gameId, p, k, v, maxResources.get(k))));
        });
    }

    public void saveGame(long gameId, TurnManager turnManager, SkillTreeManager skillTreesManager, ResourceManager resources, ModifiableGameMap map, GameRules rules, boolean firstTurn, List<Player> players, String gameName) {
        Runnable f = () -> {
            saveGameOnTheSameThread(gameId, turnManager, skillTreesManager, resources, map, rules, firstTurn, players, gameName);
        };
        executeOnDatabase(f);
    }

    public void deleteGame(final long gameId) {
        executeOnDatabase(() -> deleteGameFromSameThread(gameId));
    }

    protected void deleteGameFromSameThread(final long gameId) {
        db.getGameDao().deleteById(gameId);
        db.getCaseDao().deleteByGameId(gameId);
        db.getPlayerDao().deleteByGameId(gameId);
        db.getPlayerResourceDao().deleteByGameId(gameId);
        db.getSkillTreeAttributeDao().deleteByGameId(gameId);
        db.getStructureDao().deleteByGameId(gameId);
        db.getUnitDao().deleteByGameId(gameId);
    }

    protected void resetGameStateFromSameThread(final long gameId) {
        db.getCaseDao().deleteByGameId(gameId);
        db.getPlayerResourceDao().deleteByGameId(gameId);
        db.getSkillTreeAttributeDao().deleteByGameId(gameId);
        db.getStructureDao().deleteByGameId(gameId);
        db.getUnitDao().deleteByGameId(gameId);
    }
}

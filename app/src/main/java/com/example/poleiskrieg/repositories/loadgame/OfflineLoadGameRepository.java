package com.example.poleiskrieg.repositories.loadgame;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.poleiskrieg.database.AppDatabase;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.repositories.basegame.BaseGameRepository;
import com.example.poleiskrieg.util.C;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class OfflineLoadGameRepository extends BaseGameRepository implements LoadGameRepository {

    protected MutableLiveData<List<GameWithPlayers>> list;

    public OfflineLoadGameRepository(Application application) {
        super(application);
        this.list = new MutableLiveData<>(new ArrayList<>());
    }

    public void updateGames() {
        Callable<List<GameWithPlayers>> f = () -> {
            cleanInvalidGames();
            List<GameWithPlayers> res = this.db.getGameDao().getGamesWithPlayers().stream().filter(gwp -> gwp.gameEntity.onlineId == -1).collect(Collectors.toList());
            Log.d(C.DEBUG_TAG, String.valueOf(res));
            return res == null ? new ArrayList<>() : res;
        };
        this.list.setValue(submitOnDatabase(f));
    }

    public LiveData<List<GameWithPlayers>> getGames() {
        this.updateGames();
        return list;
    }

    protected void cleanInvalidGames() {
        db.getGameDao().getAll().forEach(gameEntity -> {
            if ((!gameEntity.isValid && gameEntity.onlineId < 0) || gameEntity.id < 0) {
                deleteGameFromSameThread(gameEntity.id);
            }
        });
    }

    public void deleteGame(long gameId){
        this.deleteGame(gameId, true);
    }

    protected void deleteGame(long gameId, boolean update) {
        Runnable r = () -> {
            db.getGameDao().getAll().forEach(gameEntity -> {
                if (gameEntity.id == gameId) {
                    deleteGameFromSameThread(gameEntity.id);
                }
            });
        };
        executeOnDatabase(r);
        if(update){
            this.updateGames();
        }
    }
}

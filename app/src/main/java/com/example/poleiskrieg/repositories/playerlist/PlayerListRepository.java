package com.example.poleiskrieg.repositories.playerlist;

import android.app.Application;
import android.util.Log;

import com.example.poleiskrieg.database.AppDatabase;
import com.example.poleiskrieg.database.converters.ClassEntityConverters;
import com.example.poleiskrieg.database.relationships.GameWithPlayers;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.repositories.basegame.BaseGameRepository;
import com.example.poleiskrieg.repositories.basegame.OnlineBaseGameRepository;
import com.example.poleiskrieg.util.C;

import java.util.Optional;
import java.util.concurrent.Future;

public class PlayerListRepository extends OnlineBaseGameRepository {

    //TODO vedere perche gli id sono sballati(il primo player ha id+1)
    public PlayerListRepository(Application application) {
        super(application);
    }

    public void addPlayer(Player player, long gameId) {
        addPlayer(player, gameId, C.NOT_LOGGED_IN);
    }

    public PlayerEntity getPlayerEntity(Player player, long gameId){
        PlayerEntity playerEntity = new PlayerEntity();
        playerEntity.id = player.getId();
        playerEntity.name = player.getName();
        playerEntity.race = player.getRace();
        playerEntity.objective = player.getObjective();
        playerEntity.gameId = gameId;
        playerEntity.playerAccountId = C.NOT_LOGGED_IN;
        return playerEntity;
    }

    public void addPlayer(Player player, long gameId, String playerAccountId) {
        submitOnDatabase(() -> {
            PlayerEntity playerEntity = ClassEntityConverters.playerToEntity(player, gameId);
            playerEntity.playerAccountId = playerAccountId;
            db.getPlayerDao().insert(playerEntity);
            return true;
        });
    }


    public GameWithPlayers getGameWithPlayers(long gameId) {
        Future<GameWithPlayers> future;
        GameWithPlayers result = null;
        try {
            future = AppDatabase.databaseWriteExecutor.submit(() -> {
                GameWithPlayers res = db.getGameDao().getGamesWithPlayersById(gameId);
                Log.d(C.DEBUG_TAG, String.valueOf(res));
                return res;
            });
            result = future.get();
        } catch (Exception e) {
            Log.d(C.DEBUG_TAG, e.toString());
        }
        if (result == null) {
            throw new IllegalArgumentException();
        }
        return result;
    }
}

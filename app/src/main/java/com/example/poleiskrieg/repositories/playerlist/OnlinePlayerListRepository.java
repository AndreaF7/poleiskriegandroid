package com.example.poleiskrieg.repositories.playerlist;

import android.app.Application;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Callable;

import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entitiesToJsonArray;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entityToJsonObject;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonObjectToEntity;

public class OnlinePlayerListRepository extends PlayerListRepository {


    public OnlinePlayerListRepository(Application application) {
        super(application);
    }

    @Override
    public void addPlayer(Player player, long gameId) {
        super.addPlayer(player, gameId, C.getLoggedPlayerUsername().isPresent() ? C.getLoggedPlayerUsername().get() : C.NOT_LOGGED_IN);
        JSONArray postParameters = new JSONArray();
        PlayerEntity playerEntity = getPlayerEntity(player, gameId);
        playerEntity.playerAccountId = C.getLoggedPlayerUsername().get();
        playerEntity.gameId = submitOnDatabase(() -> db.getGameDao().getById(gameId).onlineId);
        postParameters.put(entityToJsonObject(playerEntity));
        Log.d(C.DEBUG_TAG, "PLayer to save: " + String.valueOf(postParameters));
        //TODO make sure the player is added, don't save the game to the database

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.ADD_PLAYER_ADDRESS,
                postParameters,
                response -> {
                    try {
                        JSONObject result = response.getJSONObject(0);
                        PlayerEntity gameEntityWithOnlineId = jsonObjectToEntity(PlayerEntity.class, result);
                        Log.d(C.DEBUG_TAG, response.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
    }

    @Override
    public long createGame(long gameId) {
        //TPDP create the game
        long onlineId = submitOnDatabase(() -> db.getGameDao().getById(gameId)).onlineId;
        return createGame(gameId, onlineId);
    }

    @Override
    public long createGame(long gameId, long onlineId) {
        saveLocalGameToServer(gameId, onlineId);
        return gameId;
    }

    public void saveLocalGameToServer(long gameId, long onlineId){
        Callable<JSONObject> f = () -> {
            createGameFromSameThread(gameId, onlineId);
            JSONObject result = new JSONObject();
            GameEntity game = db.getGameDao().getById(gameId);
            JSONArray playersArray = entitiesToJsonArray(db.getPlayerDao().getByGameId(gameId));
            JSONArray caseArray = entitiesToJsonArray(db.getCaseDao().getByGameId(gameId));
            JSONArray structureArray = entitiesToJsonArray(db.getStructureDao().getByGameId(gameId));
            JSONArray unitArray = entitiesToJsonArray(db.getUnitDao().getByGameId(gameId));
            JSONArray skillTreeArray = entitiesToJsonArray(db.getSkillTreeAttributeDao().getByGameId(gameId));
            JSONArray resourcesArray = entitiesToJsonArray(db.getPlayerResourceDao().getByGameId(gameId));

            result.put(GameEntity.class.getSimpleName(), entityToJsonObject(game));
            result.put(PlayerEntity.class.getSimpleName(), playersArray);
            result.put(CaseEntity.class.getSimpleName(), caseArray);
            result.put(StructureEntity.class.getSimpleName(), structureArray);
            result.put(UnitEntity.class.getSimpleName(), unitArray);
            result.put(SkillTreeAttributeEntity.class.getSimpleName(), skillTreeArray);
            result.put(ResourceEntity.class.getSimpleName(), resourcesArray);

            return result;
        };
        JSONObject localData = submitOnDatabase(f);
        JSONArray postParameters = new JSONArray();
        try {
            postParameters.put(0, onlineId);
            postParameters.put(1, localData);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.SAVE_GAME_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, response.toString());
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
    }
}

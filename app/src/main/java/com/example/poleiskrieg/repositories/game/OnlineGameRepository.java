package com.example.poleiskrieg.repositories.game;

import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.poleiskrieg.activities.OnlineLoadGameActivity;
import com.example.poleiskrieg.database.pojos.GameInfo;
import com.example.poleiskrieg.database.tables.CaseEntity;
import com.example.poleiskrieg.database.tables.GameEntity;
import com.example.poleiskrieg.database.tables.PlayerEntity;
import com.example.poleiskrieg.database.tables.ResourceEntity;
import com.example.poleiskrieg.database.tables.SkillTreeAttributeEntity;
import com.example.poleiskrieg.database.tables.StructureEntity;
import com.example.poleiskrieg.database.tables.UnitEntity;
import com.example.poleiskrieg.logics.game.model.gamerules.GameRules;
import com.example.poleiskrieg.logics.game.model.managers.ResourceManager;
import com.example.poleiskrieg.logics.game.model.managers.SkillTreeManager;
import com.example.poleiskrieg.logics.game.model.managers.TurnManager;
import com.example.poleiskrieg.logics.game.model.map.ModifiableGameMap;
import com.example.poleiskrieg.logics.game.model.player.Player;
import com.example.poleiskrieg.network.NetworkManager;
import com.example.poleiskrieg.repositories.basegame.OnlineBaseGameRepository;
import com.example.poleiskrieg.util.C;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entitiesToJsonArray;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.entityToJsonObject;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonArrayToEntities;
import static com.example.poleiskrieg.database.converters.EntityJsonConverters.jsonObjectToEntity;

public class OnlineGameRepository extends OnlineBaseGameRepository implements GameRepository {

    private final NetworkManager networkManager;
    private final long onlineId;
    private final long gameId;
    private final MutableLiveData<GameInfo> gameInfo;
    private final Application application;
//    private static final long UPDATE_PERIOD = 2000;
//    private static final long INITIAL_DELAY = 5000;
    private final MutableLiveData<Boolean> initialized;
//    private volatile Timer timer;
//    volatile private boolean updating;
    private final int playerId;

    public OnlineGameRepository(Application application, long gameId, long onlineId, int playerId) {
        super(application);
        this.onlineId = onlineId;
        this.gameId = gameId;
        this.application = application;
        this.initialized = new MutableLiveData<>();
        this.initialized.setValue(false);
        this.networkManager = NetworkManager.getInstance(application);
        this.gameInfo = new MutableLiveData<>();
        gameInfo.setValue(getSavedGameData(gameId));
//        updating = false;
        this.playerId = playerId;
//        timer = new Timer();
    }

    /*public void init() {
        if (!initialized.getValue()) {
            updateLocalData();TODO da mettere in load game
            resumeUpdating();
        }
    }*/

//    public synchronized void resumeUpdating() {
//        if (!updating) {
//            updating = true;
//            this.timer.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    updateLocalData(); //TODO do not update if the current player is the one with the id passed in the intent
//                }
//            }, INITIAL_DELAY, UPDATE_PERIOD);
//        }
//    }

//    public synchronized void stopUpdating() {
//        if (updating) {
//            updating = false;
//            timer.cancel();
//            timer = new Timer();
//        }
//    }

    public LiveData<Boolean> isInitialized() {
        return this.initialized;
    }

    /**
     * works only if when in the game, the database is already updated
     * with the data on the server
     *
     * @return
     */
    @Override
    public LiveData<GameInfo> getSavedGameData() {
        return this.gameInfo;
    }

    @Override
    public void saveGame(GameInfo gameInfo) {
        saveGame(gameInfo.turnManager, gameInfo.skillTreesManager, gameInfo.resources, gameInfo.map, gameInfo.rules, gameInfo.firstTurn, gameInfo.players, gameInfo.gameName);
    }

    @Override
    public void saveGame(TurnManager turnManager, SkillTreeManager skillTreesManager, ResourceManager resources, ModifiableGameMap map, GameRules rules, boolean firstTurn, List<Player> players, String gameName) {
        Callable<JSONObject> f = () -> {
            super.saveGameOnTheSameThread(gameId, turnManager, skillTreesManager, resources, map, rules, firstTurn, players, gameName);
            JSONObject result = new JSONObject(); //TODO test
            //TODO testa facendo lato server con stampe
            GameEntity game = db.getGameDao().getById(gameId);//TODO vedi perche server non aggiornato
            JSONArray playersArray = entitiesToJsonArray(db.getPlayerDao().getByGameId(gameId));
            JSONArray caseArray = entitiesToJsonArray(db.getCaseDao().getByGameId(gameId));
            JSONArray structureArray = entitiesToJsonArray(db.getStructureDao().getByGameId(gameId));
            JSONArray unitArray = entitiesToJsonArray(db.getUnitDao().getByGameId(gameId));
            JSONArray skillTreeArray = entitiesToJsonArray(db.getSkillTreeAttributeDao().getByGameId(gameId));
            JSONArray resourcesArray = entitiesToJsonArray(db.getPlayerResourceDao().getByGameId(gameId));

            result.put(GameEntity.class.getSimpleName(), entityToJsonObject(game));
            result.put(PlayerEntity.class.getSimpleName(), playersArray);
            result.put(CaseEntity.class.getSimpleName(), caseArray);
            result.put(StructureEntity.class.getSimpleName(), structureArray);
            result.put(UnitEntity.class.getSimpleName(), unitArray);
            result.put(SkillTreeAttributeEntity.class.getSimpleName(), skillTreeArray);
            result.put(ResourceEntity.class.getSimpleName(), resourcesArray);

            return result;
        };
        JSONObject localData = submitOnDatabase(f);
        JSONArray postParameters = new JSONArray();
        try {
            postParameters.put(0, onlineId);
            postParameters.put(1, localData);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e);
        }
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                C.SAVE_GAME_ADDRESS,
                postParameters,
                response -> {
                    Log.d(C.DEBUG_TAG, response.toString());
                    if(turnManager.getTurnPlayer().getId() != playerId){
                        Handler mainHandler = new Handler(application.getMainLooper());
                        mainHandler.post(() -> application.startActivity(new Intent(application, OnlineLoadGameActivity.class)));
                    }
                },
                error -> Log.d(C.DEBUG_TAG, error.toString()));//TODO if error try again
        networkManager.addToRequestQueue(request);
    }

    @Override
    public void deleteGame() {
        deleteGame(gameId, onlineId);
    }

}
